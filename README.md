# Reconstruction Grasp Pipeline

Pipeline for generating grasps using 3D reconstruction + grasp optimization. See grasp_planner (`grasp_success_problem.cpp`) for optimization problem and ll4ma collision wrapper (`scene/sdf_model/manipulation.cpp`) for collision checking.

# Running Experiments:

You will need the following branches:

1. grasp_planner: sdf_model

2. ll4ma_planner: grasp_motion_plan

3. ll4ma_opt_utils: update_env

4. grasp_pipeline: real_mark

5. reconstruction_grasp_pipeline: sim

Setting up the approaches (sorry, this is not ideal, but a better system would take time we don't have):

1. Partial View: this tests our system without reconstruction, just relying on the partial view. Go to each given package/file/line combination and change accordingly:

ll4ma_opt_utils : include/ll4ma_opt_utils/plan_env_utils.hpp:7 make sure SDF_MODEL is false. src/problems/opt_problem_base_collision.cpp:5 make sure SDF_MODEL is false.

grasp_planner: include/grasp_planner/problems/grasp_success_problem.hpp:8 make sure SDF_MODEL is false. Also uncomment lines 116 and 117 and comment lines 120 and 121.

ll4ma_planner: include/ll4ma_planner/problems/ll4ma_opt_problem.hpp:9 make sure SDF_MODEL is false.

reconstruction_grasp_pipeline: launch/reconstruction_grasp_planner.launch:5 make the default set to "partial", or pass coll_mesh:=partial when launching. src/embed_client.py uncomment line 38 and comment line 37.

2. Reconstruction: this tests our system with reconstruction. Go to each given package/file/line combination and change accordingly:

ll4ma_opt_utils : include/ll4ma_opt_utils/plan_env_utils.hpp:7 make sure SDF_MODEL is true. src/problems/opt_problem_base_collision.cpp:5 make sure SDF_MODEL is true.

grasp_planner: include/grasp_planner/problems/grasp_success_problem.hpp:8 make sure SDF_MODEL is true. Also comment lines 116 and 117 and uncomment lines 120 and 121.

ll4ma_planner: include/ll4ma_planner/problems/ll4ma_opt_problem.hpp:9 make sure SDF_MODEL is true.

reconstruction_grasp_pipeline: launch/reconstruction_grasp_planner.launch:5 make the default set to "sdf", or pass coll_mesh:=sdf when launching. src/embed_client.py comment line 38 and uncomment line 37.

Between these, do full catkin clean and catkin build (will take some time).

Launch commands:

Launch the robot description? Ask Qingkai/Bala how best to do this. My guess: roslaunch ll4ma_robots_description lbr4_rviz.launch end_effector:=allegro table:=true biotac:=false rviz:=true

You know how to launch camera - do that too.

roslaunch lbr4_allegro_moveit_config lbr4_allegro_moveit.launch

roslaunch reconstruction_grasp_pipeline reconstruction_grasp_planner.launch

roslaunch grasp_pipeline grasp_pipeline_servers.launch

roslaunch grasp_pipeline grasp_pipeline_client.launch

