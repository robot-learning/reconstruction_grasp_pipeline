import numpy as np
import time
import os
import copy
import rospy
import pdb
import tensorflow as tf

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.per_process_gpu_memory_fraction = 0.2

from sensor_msgs.msg import Image, PointCloud2
import sensor_msgs.point_cloud2 as pc2
import math
import mcubes

import multiprocessing

import sys
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir('ll4ma_3d_reconstruction') + '/src/data_generation')
sys.path.append(rp.get_pkg_dir('ll4ma_3d_reconstruction') + '/src/reconstruction_sdf')
from mise import mise_voxel
from sdf_pointconv_model import get_pointconv_model, get_sdf_prediction, get_embedding_model
from voxel_cnn_model import get_voxel_cnn_model, get_voxel_prediction
from visualization import plot_3d_points
#from move_point import move_point
from voxelize_point_cloud import point_cloud_to_voxel

sys.path.append(rp.get_pkg_dir('prob_grasp_planner') + '/src/grasp_success_model')
from grasp_success_model import get_sdf_grasp_success, get_voxel_grasp_success

_VOXEL_MODEL_PATH = '/home/markvandermerwe/models/reconstruction/voxel_cnn_object_frame'
_SDF_MODEL_PATH = '/home/markvandermerwe/models/reconstruction/pointconv_mse_object_frame'

# _GRASP_MODEL_PATH = '/home/markvandermerwe/models/grasp_success/sdf_multiloss_25'
_GRASP_MODEL_PATH = '/home/markvandermerwe/models/grasp_success/sdf_scratch'
_GRASP_MODEL_SDF = True

_POINT_CLOUD_SIZE = 1000

class EmbedClient:
    '''
    Client recieves point cloud and embeds (w/ reconstruction).
    '''

    def __init__(self):
        self.manager = multiprocessing.Manager()
        self.return_dict = self.manager.dict()

    def create_reconstruction_sdf_(self, obj_cloud, mesh_path):
        # Setup model.
        get_sdf, _, _ = get_sdf_prediction(get_pointconv_model, _SDF_MODEL_PATH)

        # Bounds of 3D space to evaluate in: [-bound, bound] in each dim.
        bound = 0.8
        # Starting voxel resolution.
        initial_voxel_resolution = 32
        # Final voxel resolution.
        final_voxel_resolution = 128

        scale_obj_cloud, _, length = self.get_scaled_point_cloud(obj_cloud)
        voxel_size = (2. * bound * length) / float(final_voxel_resolution)

        idxs = np.random.choice(scale_obj_cloud.shape[0], size=_POINT_CLOUD_SIZE, replace=True)
        obj_cloud = scale_obj_cloud[idxs,:]

        # Make view specific sdf func.
        def get_sdf_query(query_points):
            return get_sdf(np.reshape(obj_cloud, (1,_POINT_CLOUD_SIZE,3)), query_points)

        mise_voxel(get_sdf_query, bound, initial_voxel_resolution, final_voxel_resolution, voxel_size, np.array([0.0,0.0,0.0]), mesh_path)

    def create_reconstruction_sdf(self, obj_cloud, mesh_path):
        p = multiprocessing.Process(target=self.create_reconstruction_sdf_, args=(obj_cloud, mesh_path))
        p.start()
        p.join()
        
    def create_reconstruction_voxel_(self, obj_cloud, mesh_path):
        voxels, voxel_size = self.vox_obj_cloud(obj_cloud)

        # THIS IS WRONG:
        assert(False)
        reconstructed_voxel = np.reshape(get_voxel(np.reshape(voxels, (1,32,32,32,1))), (32,32,32))

        # Convert to binary voxel.
        binary_voxel = copy.deepcopy(reconstructed_voxel)
        binary_voxel[reconstructed_voxel >= 0.5] = 1
        binary_voxel[reconstructed_voxel < 0.5] = 0

        # Padding to prevent holes if go up to edge.
        binary_voxel = np.pad(binary_voxel, ((1,1),(1,1),(1,1)), mode='constant')
        
        # Mesh w/ mcubes.
        vertices, triangles = mcubes.marching_cubes(binary_voxel, 0)
        # Scale mesh.
        vertices = vertices * voxel_size

        # Center mesh.
        vertices[:,0] -= voxel_size * 17
        vertices[:,1] -= voxel_size * 17
        vertices[:,2] -= voxel_size * 17

        mcubes.export_obj(vertices, triangles, mesh_path)
        
    def create_reconstruction_voxel(self, obj_cloud, mesh_path):
        p = multiprocessing.Process(target=self.create_reconstruction_voxel_, args=(obj_cloud, mesh_path))
        p.start()
        p.join()

    def create_partial_mesh(self, obj_cloud, mesh_path, verbose=False):
        voxels, voxel_size = self.vox_obj_cloud(obj_cloud, verbose)

        voxels = np.pad(voxels, ((1,1),(1,1),(1,1)), mode='constant')
        
        # Mesh these voxels.
        vertices, triangles = mcubes.marching_cubes(voxels, 0)
        vertices = vertices * voxel_size

        # Center mesh.
        vertices[:,0] -= voxel_size * (17.)
        vertices[:,1] -= voxel_size * (17.)
        vertices[:,2] -= voxel_size * (17.)

        mcubes.export_obj(vertices, triangles, mesh_path)
        
    def get_scaled_point_cloud(self, obj_cloud, verbose=False):
        # Determine scaling size.
        max_dim = max(
            np.amax(obj_cloud[:,0]) - np.amin(obj_cloud[:,0]),
            np.amax(obj_cloud[:,1]) - np.amin(obj_cloud[:,1]),
            np.amax(obj_cloud[:,2]) - np.amin(obj_cloud[:,2]),
        )

        # Scale so that max dimension is about 1.
        scale = (1.0/1.03) / max_dim
        print("Scale, ", scale)

        # Scale every point.
        scaled_obj_cloud = obj_cloud * scale

        # Down/Up Sample cloud so everything has the same # of points.
        idxs = np.random.choice(scaled_obj_cloud.shape[0], size=_POINT_CLOUD_SIZE, replace=True)
        scaled_obj_cloud = scaled_obj_cloud[idxs,:]

        # Visualize.
        if verbose:
            plot_3d_points(np.reshape(scaled_obj_cloud, (-1,3)))

        return scaled_obj_cloud, scale, (max_dim * (1.03/1.0))

    def run_sdf_model(self, obj_cloud, return_dict):
        points_sdf = tf.placeholder(tf.float32, name="point_cloud", shape=(1, 1000, 3))
        is_training_sdf = tf.placeholder(tf.bool, name="is_training", shape=())
        embedding_sdf = get_embedding_model(points_sdf, is_training_sdf, bn_decay=None, batch_size=1)

        saver_sdf = tf.train.Saver()
        sess_sdf = tf.Session(config=config)
        saver_sdf.restore(sess_sdf, os.path.join(_SDF_MODEL_PATH, 'model.ckpt'))

        sdf_embedding = sess_sdf.run(embedding_sdf, feed_dict = {
            points_sdf: obj_cloud, is_training_sdf: False,
        })
        return_dict['sdf_embedding'] = sdf_embedding

    def run_grasp_model(self, object_p, return_dict):
        # VoxelCNN:
        partial_view = tf.placeholder(tf.float32, name="partial_voxel", shape=(1,32,32,32,1))
        full_view = tf.placeholder(tf.float32, name="full_voxel", shape=(1,32,32,32,1))
        # PointSDF:
        points = tf.placeholder(tf.float32, name="point_cloud", shape=(1,1000,3))
        xyz_in = tf.placeholder(tf.float32, name="query_points", shape=(1,1,3))
        sdf_labels = tf.placeholder(tf.float32, name="query_labels", shape=(1,1,1))
        # Grasp success:
        object_size = tf.placeholder(tf.float32, name="object_size", shape=(1,3))
        grasp_config = tf.placeholder(tf.float32, name="grasp_config", shape=(1,14))
        grasp_labels = tf.placeholder(tf.float32, name="grasp_labels", shape=(1,1))
        is_training = tf.placeholder(tf.bool, name="is_training", shape=())
        # Update embedding.
        embedding_is_training = tf.placeholder(tf.bool, name="embedding_is_training", shape=())

        if _GRASP_MODEL_SDF:
            # Setup model operations.
            grasp_prediction, loss, debug, embedding = get_sdf_grasp_success(points, xyz_in, sdf_labels, None, embedding_is_training, object_size, grasp_config, grasp_labels, is_training, reconstruction_loss=False, batch_size=1)
        else:
            grasp_prediction, loss, debug, embedding = get_voxel_grasp_success(partial_view, full_view, embedding_is_training, object_size, grasp_config, grasp_labels, is_training, reconstruction_loss=False, batch_size=1)

        # Only need to restore encoder for either.
        embedding_saver = tf.train.Saver(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, 'encoder'))

        sess_grasp = tf.Session(config=config)
        embedding_saver.restore(sess_grasp, os.path.join(_GRASP_MODEL_PATH, 'model.ckpt'))

        if _GRASP_MODEL_SDF:
            points_ = object_p
            voxel_ = np.zeros((1,32,32,32,1))
        else:
            points_ = np.zeros((1,1000,3))
            voxel_ = object_p
        
        # Feeding dummy data in.
        grasp_embedding = sess_grasp.run(embedding, feed_dict = {
            partial_view: voxel_,
            full_view: np.zeros((1,32,32,32,1)),
            points: points_,
            xyz_in: np.zeros((1,1,3)),
            sdf_labels: np.zeros((1,1,1)),
            object_size: np.zeros((1,3)),
            grasp_config: np.zeros((1,14)),
            grasp_labels: np.zeros((1,1)),
            is_training: False,
            embedding_is_training: False,
        })
        return_dict['grasp_embedding'] = grasp_embedding

    def sdf_embedding(self, obj_cloud, verbose=False):
        obj_cloud_input, scale, _ = self.get_scaled_point_cloud(obj_cloud, verbose)
        
        # Embed the cloud.
        p = multiprocessing.Process(target=self.run_sdf_model, args=(np.reshape(obj_cloud_input, (1,_POINT_CLOUD_SIZE,3)), self.return_dict))
        p.start()
        p.join()
        embedding = self.return_dict['sdf_embedding']

        return embedding, scale

    def vox_obj_cloud(self, obj_cloud, verbose=False):
        # Mesh the partial view voxels.
        max_dim = max(
            np.amax(obj_cloud[:,0]) - np.amin(obj_cloud[:,0]),
            np.amax(obj_cloud[:,1]) - np.amin(obj_cloud[:,1]),
            np.amax(obj_cloud[:,2]) - np.amin(obj_cloud[:,2]),
        )
        voxel_size = max_dim / 26

        # Voxelize.
        voxels = point_cloud_to_voxel(obj_cloud, voxel_size, 26, 32, verbose)

        # Visualize.
        # viz_voxel_grid = convert_to_sparse_voxel_grid(voxels).astype(np.float64)
        # viz_voxel_grid[:,0] -= 16.
        # viz_voxel_grid[:,1] -= 16.
        # viz_voxel_grid[:,2] -= 16.
        # viz_voxel_grid *= voxel_size

        # viz_voxel_grid[:,0] += aligned_object_pose.position.x
        # viz_voxel_grid[:,1] += aligned_object_pose.position.y
        # viz_voxel_grid[:,2] += aligned_object_pose.position.z
        # self.visualize_cloud(viz_voxel_grid, 'world', 'voxel_partial')

        return voxels, voxel_size
    
    def grasp_embedding(self, obj_cloud, sdf=True, verbose=False):

        if sdf:
            # Get scaled/sampled point cloud.
            obj_cloud_input, _, _ = self.get_scaled_point_cloud(obj_cloud, verbose)
            
            p = multiprocessing.Process(target=self.run_grasp_model, args=(np.reshape(obj_cloud_input, (1, _POINT_CLOUD_SIZE, 3)), self.return_dict))
            p.start()
            p.join()
            embedding = self.return_dict['grasp_embedding']
            return embedding
        else:
            voxels, _ = self.vox_obj_cloud(obj_cloud, verbose)

            p = multiprocessing.Process(target=self.run_grasp_model, args=(np.reshape(voxels, (1,32,32,32,1)), self.return_dict))
            p.start()
            p.join()
            embedding = self.return_dict['grasp_embedding']
            return embedding
    
    def embed_point_cloud(self, obj_cloud, verbose=False):
        '''
        Given an np array point cloud in desired frame, embed it for both PointSDF and Grasp success networks.
        '''
        
        sdf_embed, sdf_scale = self.sdf_embedding(obj_cloud, verbose)
        grasp_embed = self.grasp_embedding(obj_cloud, _GRASP_MODEL_SDF, verbose)

        return sdf_embed, sdf_scale, grasp_embed
    
if __name__ == '__main__':
    embed_client = EmbedClient()
