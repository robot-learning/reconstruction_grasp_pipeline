import numpy as np
import time
import os
import copy
import rospy
import pdb
import tensorflow
from tensorflow import keras
from sensor_msgs.msg import Image, PointCloud2
import sensor_msgs.point_cloud2 as pc2
#tensorflow.enable_eager_execution()
import math

import roslib.packages as rp
import sys
sys.path.append(rp.get_pkg_dir('ll4ma_3d_reconstruction') + '/src/data_generation')
sys.path.append(rp.get_pkg_dir('ll4ma_3d_reconstruction') + '/src/reconstruction_models')

from voxel_dataset import get_voxel_dataset
from show_voxel import plot_voxel, convert_to_sparse_voxel_grid
from helper import get_f1, get_loss, lambda_binary_crossentropy, f1_prediction, get_binary_voxel
from cnn_ae import get_voxel_ae
from voxelize_point_cloud import point_cloud_to_voxel
from object_frame import find_object_frame

def predict_and_show(model, x, y, threshold):
    # Run the example.
    x_ = tensorflow.cast(x, dtype=tensorflow.float32)
    y_ = tensorflow.cast(y, dtype=tensorflow.float32)
    predict = model(x_)

    # Display the result.
    plot_voxel(convert_to_sparse_voxel_grid(np.reshape(x.numpy(), (32,32,32))), voxel_res=(32,32,32))
    plot_voxel(convert_to_sparse_voxel_grid(np.reshape(y.numpy(), (32,32,32))), voxel_res=(32,32,32))
    plot_voxel(convert_to_sparse_voxel_grid(np.reshape(predict.numpy(), (32,32,32))), voxel_res=(32,32,32))

class ReconstructionClient:
    '''
    Client recieves point cloud info, discretizes it to a voxel grid and reconstructs it.
    Can return full reconstruction or embedding.
    '''

    def __init__(self, model_file, verbose=False):
        self.model = keras.models.load_model(model_file)

        # Sanity check on model:
        # _TRAIN_FILE_DATABASE = '/home/markvandermerwe/ReconstructionData/PointCloudAligned/Train'
        # _VALIDATION_FILE_DATABASE = '/home/markvandermerwe/ReconstructionData/PointCloudAligned/Validation'

        # train_files = [os.path.join(_TRAIN_FILE_DATABASE, filename) for filename in os.listdir(_TRAIN_FILE_DATABASE) if ".tfrecord" in filename]
        # validation_files = [os.path.join(_VALIDATION_FILE_DATABASE, filename) for filename in os.listdir(_VALIDATION_FILE_DATABASE) if ".tfrecord" in filename]
        
        # # Load datasets.
        # train_dataset = get_voxel_dataset(train_files, batch_size=1)
        # validation_dataset = get_voxel_dataset(validation_files, batch_size=1)

        # # Try some from the train dataset.
        # for i, (x, y) in enumerate(train_dataset):
        #     predict_and_show(self.model, x, y, 0.5)

        #     if i == 3:
        #         break

        # # Try some from the validation dataset.
        # for i, (x, y) in enumerate(validation_dataset):
        #     predict_and_show(self.model, x, y, 0.5)

        #     if i == 3:
        #         break

    def rotation(self, theta):
        '''
        Generate 3D rotation about the x axis for the specified theta.
        '''

        rot = np.matrix([[1, 0, 0, 0], [0, math.cos(theta), math.sin(theta), 0], [0, -math.sin(theta), math.cos(theta), 0], [0, 0, 0, 1]])
        return rot
        
    def reconstruct_point_cloud(self, pc2_cloud, partial_voxel_res, full_voxel_res, object_frame, verbose=False):
        '''
        Given a pc2 point cloud, convert to voxel and run through our model to reconstruct.
        partial_voxel_res is the voxel resolution for the partial view, which is centered in a voxelization
        of resolution full_voxel_res.
        '''
        cloud = np.array(list(pc2.read_points(pc2_cloud, field_names = ("x", "y", "z"), skip_nans=True)))

        obj_cloud = cloud
        # Filter point cloud. This is a hack because the point cloud segmentation is bad.
        # for point in cloud:
        #     if point[2] > -1.1:
        #         obj_cloud.append([point[0], point[1], -point[2]])
        # obj_cloud = np.array(obj_cloud)
        
        xs = obj_cloud[:,0]
        ys = obj_cloud[:,1]
        zs = obj_cloud[:,2]

        if object_frame:
            # Setup object frame.
            object_transform, world_frame_center = find_object_frame(obj_cloud, verbose)

            # Transform our point cloud.
            for i in range(len(xs)):
                res = np.matmul(object_transform, [xs[i], ys[i], zs[i], 1])
                obj_cloud[i] = res[:3]
        else:
            # Points are not in the correct frame by default b/c of difference between
            # pyrender and Gazebo depth camera frame. Just need to rotate pi around x-axis.
            object_transform = self.rotation(math.pi)

            # Transform our point cloud.
            for i in range(len(xs)):
                res = np.matmul(object_transform, [xs[i], ys[i], zs[i], 1])
                obj_cloud[i] = np.array(res[0,:3])[0]
            
    
        # Determine voxelization size.
        max_dim = max(
            np.amax(xs) - np.amin(xs),
            np.amax(ys) - np.amin(ys),
            np.amax(zs) - np.amin(zs),
        )
        voxel_size = max_dim / partial_voxel_res

        if not object_frame:
            # Center the cloud.
            world_frame_center = [
                (np.amax(xs) + np.amin(xs)) / 2,
                (np.amax(ys) + np.amin(ys)) / 2,
                (np.amax(zs) + np.amin(zs)) / 2,
            ]
            obj_cloud[:,0] -= world_frame_center[0]
            obj_cloud[:,1] -= world_frame_center[1]
            obj_cloud[:,2] -= world_frame_center[2]

        # Convert to voxel.
        voxel = point_cloud_to_voxel(obj_cloud, voxel_size, partial_voxel_res, full_voxel_res, verbose)

        x_ = tensorflow.reshape(tensorflow.cast(voxel, dtype=tensorflow.float32), (1,full_voxel_res,full_voxel_res,full_voxel_res,1))
        predict = self.model(x_)

        reconstructed_voxel = np.reshape(predict.numpy(), (full_voxel_res,full_voxel_res,full_voxel_res))
        if verbose:
            plot_voxel(convert_to_sparse_voxel_grid(reconstructed_voxel), voxel_res=(full_voxel_res,full_voxel_res,full_voxel_res))

        return reconstructed_voxel, voxel_size, world_frame_center

if __name__=='__main__':
    rec = ReconstructionClient()
