#!/usr/bin/env python

import roslib
import rospy
import os

from geometry_msgs.msg import Pose, Quaternion, PoseStamped, PointStamped
from trajectory_msgs.msg import JointTrajectory
from sensor_msgs.msg import JointState
from gazebo_msgs.msg import ModelStates

from reconstruction_grasp_pipeline.srv import *
from point_cloud_segmentation.srv import *
from grasp_data_collection_pkg.srv import *
from ll4ma_planner.srv import *
from grasp_planner.srv import *
from ll4ma_opt_utils.srv import *

import roslib.packages as rp
import sensor_msgs.point_cloud2 as pc2
from sklearn.decomposition import PCA
import tf
import random
import copy

import mcubes

from robot_class import robotInterface
sys.path.append(rp.get_pkg_dir('hand_services') 
                + '/scripts')
from hand_client import handClient

sys.path.append(rp.get_pkg_dir('ll4ma_3d_reconstruction') + '/src/data_generation')
sys.path.append(rp.get_pkg_dir('ll4ma_3d_reconstruction') + '/src/reconstruction_voxel')

from helper import get_binary_voxel

from reconstruction_model_client import ReconstructionClient
from embed_client import EmbedClient

import pdb

_MODEL_FILE = '/home/markvandermerwe/catkin_ws/src/ll4ma_3d_reconstruction/src/reconstruction_models/full_point_aligned_ae.h5'
_MESH_SAVE_FILE = '/home/markvandermerwe/catkin_ws/src/ll4ma_3d_reconstruction/src/mesh.dae'

class ReconstructionGraspClient:

    def __init__(self):
        # Create a tf listener to handle pose transformations.
        self.tf_listener = tf.TransformListener()

        # Create robot interface to communicate trajectories.
        self.robot_interface = robotInterface(init_node=False)

        # Create hand client.
        self.hand_client = handClient()

        self.object_meshes_path = '/home/markvandermerwe/YCB/ycb_meshes'

    def get_pose_stamped_from_array(self, pose_array, frame_id='/world'):
        pose_stamped = PoseStamped()
        pose_stamped.header.frame_id = frame_id 

        pose_quaternion = tf.transformations.quaternion_from_euler(pose_array[0], pose_array[1], pose_array[2])
        pose_stamped.pose.orientation.x, pose_stamped.pose.orientation.y, \
                pose_stamped.pose.orientation.z, pose_stamped.pose.orientation.w = pose_quaternion 
        pose_stamped.pose.position.x, pose_stamped.pose.position.y, pose_stamped.pose.position.z = \
                pose_array[3:]
        return pose_stamped
        
    def insert_object(self, object_name):
        '''
        Insert an object.
        '''
        object_pose_array = [0., 0., 0., 0., -0.8, 0.59]

        rospy.loginfo('Waiting for service update_gazebo_object.')
        rospy.wait_for_service('update_gazebo_object')
        rospy.loginfo('Calling service update_gazebo_object.')
        try:
            update_object_gazebo_proxy = rospy.ServiceProxy('update_gazebo_object', UpdateObjectGazebo)
            update_object_gazebo_request = UpdateObjectGazeboRequest()
            update_object_gazebo_request.object_name = object_name
            update_object_gazebo_request.object_pose_array = object_pose_array
            update_object_gazebo_request.object_model_name = object_name
            update_object_gazebo_response = update_object_gazebo_proxy(update_object_gazebo_request) 
            #print update_object_gazebo_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service update_gazebo_object call failed: %s'%e)
        rospy.loginfo('Service update_gazebo_object is executed %s.'%str(update_object_gazebo_response))

        return update_object_gazebo_response.success, object_pose_array

    def remove_objects(self, object_names):
        rospy.loginfo('Waiting for service remove_gazebo_object.')
        rospy.wait_for_service('remove_gazebo_object')
        rospy.loginfo('Calling service remove_gazebo_object.')
        try:
            remove_object_gazebo_proxy = rospy.ServiceProxy('remove_gazebo_object', RemoveObjectGazebo)
            remove_object_gazebo_request = RemoveObjectGazeboRequest()
            remove_object_gazebo_request.object_names = object_names
            remove_object_gazebo_response = remove_object_gazebo_proxy(remove_object_gazebo_request)
        except rospy.ServiceException, e:
            rospy.loginfo('Service remove_gazebo_object call failed: %s'%e)
        rospy.loginfo('Service remove_gazebo_object is executed %s.'%str(remove_object_gazebo_response))
        return remove_object_gazebo_response.success

    def add_object_grasp_planner(self, object_mesh_file, object_pose_lbr4):

        quaternion = [
            object_pose_lbr4.orientation.x,
            object_pose_lbr4.orientation.y,
            object_pose_lbr4.orientation.z,
            object_pose_lbr4.orientation.w,            
        ]
        euler = tf.transformations.euler_from_quaternion(quaternion)
        
        ll4ma_planner_pose = [
            object_pose_lbr4.position.x,
            object_pose_lbr4.position.y,
            object_pose_lbr4.position.z,
            euler[0],
            euler[1],
            euler[2],
        ]
        print ll4ma_planner_pose
        return self.update_grasp_planner(object_mesh_file, ll4ma_planner_pose)

    def remove_objects(self, object_names):
        rospy.loginfo('Waiting for service remove_gazebo_object.')
        rospy.wait_for_service('remove_gazebo_object')
        rospy.loginfo('Calling service remove_gazebo_object.')
        try:
            remove_object_gazebo_proxy = rospy.ServiceProxy('remove_gazebo_object', RemoveObjectGazebo)
            remove_object_gazebo_request = RemoveObjectGazeboRequest()
            remove_object_gazebo_request.object_names = object_names
            remove_object_gazebo_response = remove_object_gazebo_proxy(remove_object_gazebo_request)
        except rospy.ServiceException, e:
            rospy.loginfo('Service remove_gazebo_object call failed: %s'%e)
        rospy.loginfo('Service remove_gazebo_object is executed %s.'%str(remove_object_gazebo_response))
        return remove_object_gazebo_response.success

    def update_grasp_planner(self, object_mesh_file, pose):

        rospy.loginfo('Waiting for service /grasp_planner/add_object')
        rospy.wait_for_service('/grasp_planner/add_object')
        rospy.loginfo('Calling add_object service.')

        try:
            add_object_proxy = rospy.ServiceProxy('/grasp_planner/add_object', AddObject)

            add_object_request = AddObjectRequest()
            add_object_request.obj_mesh_file = object_mesh_file
            add_object_request.obj_pose = pose
            add_object_request.obj_pose_offset = Pose()
            add_object_response = add_object_proxy(add_object_request)
        except rospy.ServiceException, e:
            rospy.loginfo('Service add_object call failed: %s'%e)
        rospy.loginfo('Service add_object is executed %s.'%str(add_object_response))

        # rospy.loginfo('Waiting for service /grasp_planner/add_object')
        # rospy.wait_for_service('/grasp_planner/add_object')
        # rospy.loginfo('Calling add_object service.')

        # try:
        #     add_object_proxy = rospy.ServiceProxy('/grasp_planner/add_object', AddObject)

        #     add_object_request = AddObjectRequest()
        #     add_object_request.obj_mesh_file = full_mesh_file
        #     add_object_request.obj_pose = pose

        #     add_object_response = add_object_proxy(add_object_request)
        # except rospy.ServiceException, e:
        #     rospy.loginfo('Service add_object call failed: %s'%e)
        # rospy.loginfo('Service add_object is executed %s.'%str(add_object_response))
        
        return add_object_response.success

    def get_joint_states(self):
        '''
        Return the current arm and hand position.
        '''
        current_joint_state = rospy.wait_for_message('/joint_states', JointState)

        robot_js=JointState()
        robot_js.name=['lbr4_j0','lbr4_j1','lbr4_j2','lbr4_j3','lbr4_j4','lbr4_j5','lbr4_j6']
        robot_js.position=current_joint_state.position[0:7]

        hand_js=JointState()
        hand_js.name=['index_joint_0','index_joint_1','index_joint_2','index_joint_3',
                      'middle_joint_0','middle_joint_1', 'middle_joint_2','middle_joint_3',
                      'ring_joint_0', 'ring_joint_1', 'ring_joint_2', 'ring_joint_3',
                      'thumb_joint_0', 'thumb_joint_1', 'thumb_joint_2','thumb_joint_3']
        hand_js.position=current_joint_state.position[7:]

        return robot_js, hand_js

    def get_table_pose(self):
        '''
        Get pose of the table in the world frame.
        '''
        print "Waiting for model state"
        current_table_pose = rospy.wait_for_message('/gazebo/model_states', ModelStates)

        current_table_pose_stamped = PoseStamped()
        current_table_pose_stamped.pose = current_table_pose.pose[1]
        current_table_pose_stamped.header.frame_id = 'world'

        table_pose_lbr4 = self.tf_listener.transformPose('lbr4_base_link', current_table_pose_stamped).pose

        quaternion = [
            table_pose_lbr4.orientation.x,
            table_pose_lbr4.orientation.y,
            table_pose_lbr4.orientation.z,
            table_pose_lbr4.orientation.w,            
        ]
        euler = tf.transformations.euler_from_quaternion(quaternion)
        
        return [
            table_pose_lbr4.position.x,
            table_pose_lbr4.position.y,
            table_pose_lbr4.position.z,
            euler[0],
            euler[1],
            euler[2],
        ]

    def gen_grasp_preshape_client(self, object_segment_response):
        rospy.loginfo('Waiting for service gen_grasp_preshape.')
        rospy.wait_for_service('gen_grasp_preshape')
        rospy.loginfo('Calling service gen_grasp_preshape.')
        try:
            preshape_proxy = rospy.ServiceProxy('gen_grasp_preshape', GraspPreshape)
            preshape_request = GraspPreshapeRequest()
            #if not self.use_sim:
            #    preshape_request.obj = self.object_segment_response.obj
            #else:
                #preshape_request.obj = self.object_segment_blensor_response.obj
            preshape_request.obj = object_segment_response.obj

            # Testing in simulation
            #preshape_req.obj = self.object_segment_response.obj
            preshape_response = preshape_proxy(preshape_request) 
            #self.preshape_palm_goal_pose_sd_pcd = self.listener.transformPose(self.kinect2_sd_frame_id, 
            #                                    self.preshape_response.palm_goal_pose_in_pcd)
            #print self.preshape_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service gen_grasp_preshape call failed: %s'%e)
            return None
        rospy.loginfo('Service gen_grasp_preshape is executed.')
        return preshape_response

    def segment_object_client(self):
        rospy.loginfo('Waiting for service object_segmenter.')
        rospy.wait_for_service('object_segmenter')
        rospy.loginfo('Calling service object_segmenter.')
        try:
            object_segment_proxy = rospy.ServiceProxy('object_segmenter', SegmentGraspObject)
            object_segment_request = SegmentGraspObjectRequest()
            object_segment_response = object_segment_proxy(object_segment_request) 
            #print self.object_segment_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service object_segmenter call failed: %s'%e)
        rospy.loginfo('Service object_segmenter is executed.')
        return object_segment_response

    def control_allegro_config_client(self, allegro_joint_state):
        rospy.loginfo('Waiting for service control_allegro_config.')
        rospy.wait_for_service('control_allegro_config')
        rospy.loginfo('Calling service control_allegro_config.')
        try:
            control_proxy = rospy.ServiceProxy('control_allegro_config', AllegroConfig)
            control_request = AllegroConfigRequest()
            control_request.allegro_target_joint_state = allegro_joint_state
            control_response = control_proxy(control_request) 
        except rospy.ServiceException, e:
            rospy.loginfo('Service control_allegro_config call failed: %s'%e)
        rospy.loginfo('Service control_allegro_config is executed %s.'%str(control_response))
        return control_response.success

    def get_grasp_plan(self, arm_j0, hand_j0, hand_shape):
        '''
        Use Bala heuristic to get an init trajectory + palm pose.
        '''
        rospy.loginfo('Waiting for service grasp_planner/get_grasp_plan.')
        rospy.wait_for_service('/grasp_planner/get_grasp_plan')
        rospy.loginfo('Calling service. /grasp_planner/get_grasp_plan')
        try:
            palm_plan_proxy = rospy.ServiceProxy('/grasp_planner/get_grasp_plan', FtipGrasp)
            palm_plan_request = FtipGraspRequest()
            palm_plan_request.arm_jstate = arm_j0
            palm_plan_request.hand_jstate = hand_j0
            palm_plan_request.hand_preshape = hand_shape
            palm_plan_request.t_steps = 1 # Just need final pose, rest is planned by the ll4ma_planner.
            palm_plan_request.optimize = True
            palm_plan_request
            palm_plan_response = palm_plan_proxy(palm_plan_request) 
        except rospy.ServiceException, e:
            rospy.loginfo('Service /grasp_planner/get_grasp_plan call failed: %s'%e)
        return palm_plan_response
    
    # Stolen from: grasp_planner/scripts/grasp_client.py.
    def get_palm_plan(self, arm_j0,hand_j0,palm_poses,hand_shape,t_steps=1,object_pose=Pose(),init_traj=JointTrajectory()):
        # get plan from service:
        rospy.loginfo('waiting for ll4ma planner')
        #print "waiting for call"
        rospy.wait_for_service('/ll4ma_planner/get_trajectory')
        rospy.loginfo('calling ll4ma planner')
        planner=rospy.ServiceProxy('/ll4ma_planner/get_trajectory', GetEEPlan)
        get_plan_request = GetEEPlanRequest()
        get_plan_request.arm_jstate = arm_j0
        get_plan_request.hand_jstate = hand_j0
        get_plan_request.t_steps = t_steps
        get_plan_request.des_ee_pose = palm_poses
        get_plan_request.object_pose = object_pose
        get_plan_request.ee_preshape = hand_shape
        get_plan_request.initial_jtraj = init_traj
        get_plan_request.optimize = True
        resp=planner(get_plan_request)
        return resp

    def get_palm_plan_traj(self, robot_js, hand_js, palm_pose, hand_preshape):
        '''
        Given joints for robot, hand, palm pose, and hand preshape, run and 
        generate collision free trajectories.
        '''
        resp = self.get_palm_plan(robot_js, hand_js, [palm_pose], hand_preshape)
        resp = self.get_palm_plan(robot_js, hand_js, [palm_pose], hand_preshape, t_steps=10, init_traj=resp.robot_joint_traj)

        # TODO: Check result for collisions, etc.

        return resp.arm_joint_traj

    def hand_grasp_control(self):
        joint_idx = [1, 2, 3, 5, 6, 7, 9, 10, 11, 14, 15]
        self.hand_client.grasp_object(joint_idx)
        cur_js = self.hand_client.get_joint_state() 
        increase_stiffness_times = 3
        for i in xrange(increase_stiffness_times):
            _, cur_js = self.hand_client.increase_stiffness(joint_idx, cur_js)

    def lift_task_vel_planner_client(self, height_to_lift=0.15):
        rospy.loginfo('Waiting for service straight_line_planner to lift.')
        rospy.wait_for_service('straight_line_planner')
        rospy.loginfo('Calling service straight_line_planner to lift.')
        try:
            planning_proxy = rospy.ServiceProxy('straight_line_planner', StraightLinePlan)
            planning_request = StraightLinePlanRequest()
            planning_request.lift_height = height_to_lift
            response = planning_proxy(planning_request) 
        except rospy.ServiceException, e:
            rospy.loginfo('Service straight_line_planner call to lift failed: %s'%e)
            rospy.loginfo('Service straight_line_planner to lift is executed %s.'
                          %str(response.success))
        return response.plan_traj

    def execute_arm_plan(self, plan_traj, send_cmd_manually=False):

        # Smooth trajectory.
        plan_traj = self.robot_interface.get_smooth_traj(plan_traj)

        # TODO: Check for success in the plan.

        if send_cmd_manually:
            #self.palm_planner.plot_traj(plan_traj)

            # send to robot:
            send_cmd = raw_input('send to robot? (y/n)')
            if(send_cmd == 'y'):
                self.robot_interface.send_jtraj(plan_traj)
                #TO DO: make sure the robot finishing executing the trajectory before returning.
                return True
            return False
        else:
            # send to robot:
            self.robot_interface.send_jtraj(plan_traj)
            return True

    def move_arm_home(self):
        rospy.loginfo('Move the arm to go home.')
        robot_js, hand_js = self.get_joint_states()
        hand_final_shape = copy.deepcopy(hand_js)

        home_pose = Pose()
        home_pose.position.x = -0.020804
        home_pose.position.y = -0.001498
        home_pose.position.z = 1.33069
        home_pose.orientation.x = -0.007979
        home_pose.orientation.y = 0.0
        home_pose.orientation.z = 1.0
        home_pose.orientation.w = 0.0

        arm_traj = self.get_palm_plan_traj(robot_js, hand_js, home_pose, hand_final_shape)
        self.execute_arm_plan(arm_traj, send_cmd_manually=False)

    def perform_grasp(self, robot_js, hand_js, palm_pose, hand_preshape):
        '''
        Do motion planning for given object + grasp preshape using
        trajectory optimization (ee_pose_problem in ll4ma_planner).
        '''

        # Get arm trajectory.
        arm_traj = self.get_palm_plan_traj(robot_js, hand_js, palm_pose, hand_preshape)

        # Move hand to preshape
        self.control_allegro_config_client(hand_preshape)

        # Send arm trajectory.
        x = raw_input("Send plan? y/n")
        if x == 'y':
            self.execute_arm_plan(arm_traj, send_cmd_manually=True)

            # Close hand.
            raw_input("close hand?")
            self.hand_grasp_control()

            # Lift.
            raw_input("lift?")
            lift_traj = self.lift_task_vel_planner_client()
            self.execute_arm_plan(lift_traj, send_cmd_manually=True)

            # Drop.
            raw_input("drop?")
            self.control_allegro_config_client(hand_preshape)
            
            # Reset.
            raw_input("go home?")
            self.move_arm_home()

        return arm_traj
    
if __name__ == '__main__':

    rospy.init_node('reconstruction_grasp_client')

    # Make client to perform the grasps.
    grasp_client = ReconstructionGraspClient()

    # Make client for reconstructing the model.
    #reconstruct_client = ReconstructionClient(_MODEL_FILE)
    #embed_client = EmbedClient()
    
    # Object dataset (YCB)
    dataset_dir = '/home/markvandermerwe/YCB/ycb'
    object_mesh_dirs = os.listdir(dataset_dir)
    #random.shuffle(object_mesh_dirs)
    object_mesh_dirs = ['002_master_chef_can', '004_sugar_box']

    for i, object_name in enumerate(object_mesh_dirs):
        rospy.loginfo('Object: %s' %object_name)

        # Insert the object into the scene.
        insert_success, object_pose = grasp_client.insert_object(object_name)
        if not insert_success:
            print("Problem inserting object. Continuing.")
            continue
        
        # Segment object in scene.
        object_segment_response = grasp_client.segment_object_client()
        retr = None
        while object_segment_response is None:
            retr = raw_input("retry? y/n")
            if retr == 'y':
                object_segment_response = grasp_client.segment_object_client()
            else:
                break
        if retr is not None and retr != 'y':
            continue

        # Embed the object.
        # embed_client.embed_point_cloud(object_segment_response.obj.cloud, True)
        
        # Send object cloud to be reconstructed.
        # reconstructed_voxel, voxel_size, centroid = reconstruct_client.reconstruct_point_cloud(object_segment_response.obj.cloud, 20, 32, False)
        # reconstructed_voxel = get_binary_voxel(reconstructed_voxel, 0.5)
        
        # # Create a mesh from the reconstructed voxels.
        # vertices, triangles = mcubes.marching_cubes(reconstructed_voxel, 0)
        # # Center and scale mesh based on voxel sizes.
        # vertices = vertices * voxel_size
        # mcubes.export_mesh(vertices, triangles, _MESH_SAVE_FILE, 'reconstructed_mesh')

        # # Convert the object pose in the point cloud frame to the lbr4_base_frame.
        # object_pose = PoseStamped()
        # object_pose.header.frame_id = 'kinect_pts'
        # object_pose.pose.position.x = centroid[0] - (16 * voxel_size)
        # object_pose.pose.position.y = centroid[1] - (16 * voxel_size)
        # object_pose.pose.position.z = centroid[2] - (16 * voxel_size)
        # object_pose = grasp_client.tf_listener.transformPose('lbr4_base_link', object_pose).pose
        
        # # Add reconstructed mesh into planner.
        # grasp_client.add_object_ll4ma_planner(_MESH_SAVE_FILE, object_pose)
        # obj_pose = grasp_client.get_pose_stamped_from_array(object_pose)
        # obj_pose = grasp_client.tf_listener.transformPose('lbr4_base_link', obj_pose).pose
        
        # grasp_client.update_grasp_planner(os.path.join('/home/markvandermerwe/YCB/ycb_meshes', object_name + '.stl'), obj_pose)
        
        # Just generate a preshape here, we ignore pose. TODO: Just setup a default shape.        
        preshape_response = grasp_client.gen_grasp_preshape_client(object_segment_response)
        if preshape_response is None:
            continue
        # Grab a preshape.
        hand_preshape = preshape_response.allegro_joint_state[0]
        palm_pose = preshape_response.palm_goal_pose_in_pcd[0]
        palm_pose_robot_frame = grasp_client.tf_listener.transformPose('lbr4_base_link', palm_pose).pose

        print hand_preshape

        # Get current hand/robot joint states.
        robot_js, hand_js = grasp_client.get_joint_states()

        # Call grasp_planner to get a grasp configuration.
        palm_plan_response = grasp_client.get_grasp_plan(robot_js, hand_js, hand_preshape)
        if palm_plan_response is None:
            continue

        # Palm pose from the heuristic should be in desired frame already.
        palm_pose = palm_plan_response.palm_pose

        # TODO: Add init traj from palm plan.
        grasp_client.perform_grasp(robot_js, hand_js, palm_pose, hand_preshape)
        
        raw_input("Wait.")
        
        # Remove the object from the scene.
        # grasp_client.remove_objects([object_name])
        
