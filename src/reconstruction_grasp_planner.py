#!/usr/bin/env python
# Given point cloud (and if in simulation, ground truth mesh information), generate
# a grasp using our learned reconstruction + constrained optimization.

import roslib
roslib.load_manifest('reconstruction_grasp_pipeline')
import rospy
import os

from geometry_msgs.msg import Pose, Quaternion, PoseStamped, PointStamped, Point
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sensor_msgs.msg import JointState
from gazebo_msgs.msg import ModelStates
from moveit_msgs.msg import DisplayTrajectory, RobotTrajectory
from visualization_msgs.msg import Marker

from grasp_pipeline.srv import GetGraspPlan, GetGraspPlanRequest, GetGraspPlanResponse
from point_cloud_segmentation.srv import *
from ll4ma_planner.srv import *
from grasp_planner.srv import *
from ll4ma_opt_utils.srv import *

import roslib.packages as rp
import sensor_msgs.point_cloud2 as pc2
from sklearn.decomposition import PCA
import tf
import random
import copy
import numpy as np
import time
import pickle

import mcubes

from urdf_parser_py.urdf import URDF
from pykdl_utils.kdl_kinematics import KDLKinematics

from robot_class import robotInterface
sys.path.append(rp.get_pkg_dir('hand_services') 
                + '/scripts')
from hand_client import handClient

sys.path.append(rp.get_pkg_dir('ll4ma_3d_reconstruction') + '/src/data_generation')
sys.path.append(rp.get_pkg_dir('ll4ma_3d_reconstruction') + '/src/reconstruction_voxel')
sys.path.append(rp.get_pkg_dir('prob_grasp_planner') + '/src/grasp_common_library')

from show_voxel import convert_to_sparse_voxel_grid
from object_frame import find_object_frame
from voxelize_point_cloud import point_cloud_to_voxel
from embed_client import EmbedClient
#from full_client import SDFClient

import pdb

class ReconstructionGraspPlanner:

    def __init__(self):
        # Create a tf listener to handle pose transformations.
        self.tf_listener = tf.TransformListener()

        # Qingkai's grasp prior model. Used to initialize optimization.
        prior_model_path = '/home/markvandermerwe/models/GraspSuccessReconstruct/Prior/preshape_side_prior.model'
        self.prior_model = pickle.load(open(prior_model_path, 'rb'))

        # Hand joint angles we care about.
        self.preshape_joints = rospy.get_param("grasp_planner/preshape_joints", None)
        self.joint_names = rospy.get_param("grasp_planner/joint_names", None)
        self.coll_mesh_approach = rospy.get_param("/collision_mesh", "partial")

        self.coll_id = 0

        assert(self.preshape_joints is not None)
        assert(self.joint_names is not None)

    def write_obj_info_to_yaml(self, filename, obj_pose_sdf, obj_pose, obj_size, grasp_embedding, sdf_embedding, scale, mesh_file, mesh_pose, sdf_whd, partial_mesh, partial_mesh_pose):
        '''
        Create a yaml file for this object and write all the info to it.
        '''
        with open(filename, 'w') as f:
            f.write("ll4ma_scene:\n")
            f.write("  true_mesh: \"" + str(mesh_file) + "\"\n")
            f.write("  true_pose: " + str(mesh_pose) + "\n")
            f.write("  partial_mesh: \"" + str(partial_mesh) + "\"\n")
            f.write("  partial_pose: " + str(partial_mesh_pose) + "\n")
            f.write("  grasp_pose: " + str(obj_pose) + "\n")
            f.write("  grasp_size: " + str(obj_size) + "\n")
            f.write("  grasp_embedding: " + str(grasp_embedding) + "\n")
            f.write("  sdf_scale: " + str(scale) + "\n")
            f.write("  sdf_pose: " + str(obj_pose_sdf) + "\n")
            f.write("  sdf_size: " + str(sdf_whd) + "\n")
            f.write("  sdf_embedding: " + str(sdf_embedding) + "\n")

    def update_ll4ma_planner(self, sdf_embedding, sdf_scale, sdf_pose, sdf_whd, grasp_embedding, grasp_size, grasp_pose, true_mesh, true_pose, partial_mesh, partial_mesh_pose):
        rospy.loginfo('Waiting for service /ll4ma_planner/add_object')
        rospy.wait_for_service('/ll4ma_planner/add_object')
        rospy.loginfo('Calling add_object service.')

        try:
            add_object_proxy = rospy.ServiceProxy('/ll4ma_planner/add_object', AddObject)

            add_object_request = AddObjectRequest()
            add_object_request.sdf_embedding = sdf_embedding
            add_object_request.sdf_scale = sdf_scale
            add_object_request.sdf_pose = sdf_pose
            add_object_request.sdf_size = sdf_whd
            add_object_request.grasp_embedding = grasp_embedding
            add_object_request.grasp_size = grasp_size
            add_object_request.grasp_pose = grasp_pose
            add_object_request.true_mesh = true_mesh
            add_object_request.true_pose = true_pose
            add_object_request.partial_mesh = partial_mesh
            add_object_request.partial_mesh_pose = partial_mesh_pose
            add_object_response = add_object_proxy(add_object_request)
        except rospy.ServiceException, e:
            rospy.loginfo('Service add_object call failed: %s'%e)
        rospy.loginfo('Service add_object is executed %s.'%str(add_object_response))
        
        return add_object_response.success
    
    def update_grasp_planner(self, sdf_embedding, sdf_scale, sdf_pose, sdf_whd, grasp_embedding, grasp_size, grasp_pose, true_mesh, true_pose, partial_mesh, partial_mesh_pose):

        rospy.loginfo('Waiting for service /grasp_planner/add_object')
        rospy.wait_for_service('/grasp_planner/add_object')
        rospy.loginfo('Calling add_object service.')

        try:
            add_object_proxy = rospy.ServiceProxy('/grasp_planner/add_object', AddObject)

            add_object_request = AddObjectRequest()
            add_object_request.sdf_embedding = sdf_embedding
            add_object_request.sdf_scale = sdf_scale
            add_object_request.sdf_pose = sdf_pose
            add_object_request.sdf_size = sdf_whd
            add_object_request.grasp_embedding = grasp_embedding
            add_object_request.grasp_size = grasp_size
            add_object_request.grasp_pose = grasp_pose
            add_object_request.true_mesh = true_mesh
            add_object_request.true_pose = true_pose
            add_object_request.partial_mesh = partial_mesh
            add_object_request.partial_mesh_pose = partial_mesh_pose
            add_object_response = add_object_proxy(add_object_request)
        except rospy.ServiceException, e:
            rospy.loginfo('Service add_object call failed: %s'%e)
            return False
        rospy.loginfo('Service add_object is executed %s.'%str(add_object_response))
        
        return add_object_response.success

    def get_joint_states(self):
        '''
        Return the current arm and hand position.
        '''
        current_joint_state = rospy.wait_for_message('/lbr4_allegro/joint_states', JointState)

        robot_js=JointState()
        robot_js.name=['lbr4_j0','lbr4_j1','lbr4_j2','lbr4_j3','lbr4_j4','lbr4_j5','lbr4_j6']
        robot_js.position=current_joint_state.position[0:7]

        hand_js=JointState()
        hand_js.name=['index_joint_0','index_joint_1','index_joint_2','index_joint_3',
                      'middle_joint_0','middle_joint_1', 'middle_joint_2','middle_joint_3',
                      'ring_joint_0', 'ring_joint_1', 'ring_joint_2', 'ring_joint_3',
                      'thumb_joint_0', 'thumb_joint_1', 'thumb_joint_2','thumb_joint_3']
        hand_js.position=current_joint_state.position[7:]

        return robot_js, hand_js
    
    def get_joint_states_gazebo(self):
        '''
        Return the current arm and hand position.
        '''
        arm_joint_state = rospy.wait_for_message('/lbr4/joint_states', JointState)
        hand_joint_state = rospy.wait_for_message('/allegro_hand_right/joint_states', JointState)

        return arm_joint_state, hand_joint_state

    def get_grasp_plan(self, arm_j0, hand_j0, hand_shape, init_traj=JointTrajectory()):
        '''
        Run grasp planner.
        '''
        rospy.loginfo('Waiting for service grasp_planner/get_grasp_plan.')
        rospy.wait_for_service('/grasp_planner/get_grasp_plan')
        rospy.loginfo('Calling service. /grasp_planner/get_grasp_plan')
        try:
            palm_plan_proxy = rospy.ServiceProxy('/grasp_planner/get_grasp_plan', FtipGrasp)
            palm_plan_request = FtipGraspRequest()
            palm_plan_request.arm_jstate = arm_j0
            palm_plan_request.hand_jstate = hand_j0
            palm_plan_request.hand_preshape = hand_shape
            palm_plan_request.t_steps = 1 # Just need final pose, rest is planned by the ll4ma_planner.
            palm_plan_request.optimize = True
            palm_plan_request.initial_jtraj = init_traj
            palm_plan_response = palm_plan_proxy(palm_plan_request) 
        except rospy.ServiceException, e:
            rospy.loginfo('Service /grasp_planner/get_grasp_plan call failed: %s'%e)
            return None
        return palm_plan_response

    def get_ik(self, arm_j0, hand_j0, palm_poses, hand_shape):
        # get plan from service:
        rospy.loginfo('waiting for ik planner')
        #print "waiting for call"
        rospy.wait_for_service('/ik_planner/get_trajectory')
        rospy.loginfo('calling ik planner')
        planner=rospy.ServiceProxy('/ik_planner/get_trajectory', GetEEPlan)
        get_plan_request = GetEEPlanRequest()
        get_plan_request.arm_jstate = arm_j0
        get_plan_request.hand_jstate = hand_j0
        get_plan_request.t_steps = 1
        get_plan_request.des_ee_pose = palm_poses
        get_plan_request.object_pose = Pose()
        get_plan_request.ee_preshape = hand_shape
        get_plan_request.initial_jtraj = JointTrajectory()
        get_plan_request.optimize = True
        resp=planner(get_plan_request)
        return resp
    
    # Stolen from: grasp_planner/scripts/grasp_client.py.
    def get_palm_plan(self, arm_j0,hand_j0,palm_poses,hand_shape,t_steps=1,object_pose=Pose(),init_traj=JointTrajectory()):
        # get plan from service:
        rospy.loginfo('waiting for ll4ma planner')
        #print "waiting for call"
        rospy.wait_for_service('/ll4ma_planner/get_trajectory')
        rospy.loginfo('calling ll4ma planner')
        planner=rospy.ServiceProxy('/ll4ma_planner/get_trajectory', GetEEPlan)
        get_plan_request = GetEEPlanRequest()
        get_plan_request.arm_jstate = arm_j0
        get_plan_request.hand_jstate = hand_j0
        get_plan_request.t_steps = t_steps
        get_plan_request.des_ee_pose = palm_poses
        get_plan_request.object_pose = object_pose
        get_plan_request.ee_preshape = hand_shape
        get_plan_request.initial_jtraj = init_traj
        get_plan_request.optimize = True
        #get_plan_request.go_home = False
        resp=planner(get_plan_request)
        return resp

    def get_palm_plan_traj(self, robot_js, hand_js, palm_pose, hand_preshape, init_traj):
        '''
        Given joints for robot, hand, palm pose, and hand preshape, run and 
        generate collision free trajectories.
        '''
        #resp = self.get_palm_plan(robot_js, hand_js, [palm_pose], hand_preshape)
        resp = self.get_palm_plan(robot_js, hand_js, [palm_pose], hand_preshape, t_steps=10, init_traj=init_traj)

        # TODO: Check result for collisions, etc.

        return resp

    def display_trajectory(self, trajectory, pub_name):
        # Display the final grasp plan.
        pub = rospy.Publisher(pub_name, DisplayTrajectory, queue_size=1)
        traj_ = DisplayTrajectory()
        traj_.model_id= 'lbr4'
        robot_traj_ = RobotTrajectory()
        robot_traj_.joint_trajectory = trajectory
        traj_.trajectory.append(robot_traj_)
    
        # while True:
        rate = rospy.Rate(10)
        for i in range(30):
            pub.publish(traj_)
            rate.sleep()

    def get_trajectory_from_joint_angles(self, joint_angles):
        new_traj = JointTrajectory()
        new_traj.header.frame_id = "/lbr4_base_link"
        new_traj.joint_names = self.joint_names

        new_pt = JointTrajectoryPoint()
        new_pt.positions = joint_angles

        new_traj.points.append(new_pt)
        return new_traj
            
    def sample_grasp_config(self):
        '''
        Get a grasp preshape config from prior model to seed grasp optimization.
        '''
        self.prior_model.random_state = None
        # if self.prior_model.random_state != None:
        #     self.prior_model.random_state = None
        return self.prior_model.sample()

    def sample_grasp_preshape(self, object_pose):
        '''
        Sample a configuration and convert to a full preshape via pykdl.
        '''
        config = self.sample_grasp_config()[0][0]

        # Convert object pose to transformation from object to lbr4 base.
        obj_lbr4_transform = tf.transformations.quaternion_matrix([object_pose[3], object_pose[4], object_pose[5], object_pose[6]])
        obj_lbr4_transform[0,3] = object_pose[0]
        obj_lbr4_transform[1,3] = object_pose[1]
        obj_lbr4_transform[2,3] = object_pose[2]

        # Get transformation from grasp to object.
        grasp_obj_transform = tf.transformations.euler_matrix(config[3], config[4], config[5])
        grasp_obj_transform[0,3] = config[0]
        grasp_obj_transform[1,3] = config[1]
        grasp_obj_transform[2,3] = config[2]

        # Combine transformations to get grasp pose in lbr4 frame.
        grasp_lbr4_transform = np.dot(obj_lbr4_transform, grasp_obj_transform)

        # Pull out grasp pose.
        grasp_pose = Pose()
        grasp_pose.position.x = grasp_lbr4_transform[0,3]
        grasp_pose.position.y = grasp_lbr4_transform[1,3]
        grasp_pose.position.z = grasp_lbr4_transform[2,3]
        grasp_pose.orientation.x, grasp_pose.orientation.y, grasp_pose.orientation.z, grasp_pose.orientation.w = tf.transformations.quaternion_from_matrix(grasp_lbr4_transform)

        # Visualize pose.
        self.visualize_pose(grasp_pose, "lbr4_base_link")

        # Get IK from ll4ma_planner.

        # Get current hand/robot joint states.
        robot_js, hand_js = self.get_joint_states()

        preshape=JointState()
        preshape.name=['index_joint_0','index_joint_1','index_joint_2','index_joint_3',
                      'middle_joint_0','middle_joint_1', 'middle_joint_2','middle_joint_3',
                      'ring_joint_0', 'ring_joint_1', 'ring_joint_2', 'ring_joint_3',
                      'thumb_joint_0', 'thumb_joint_1', 'thumb_joint_2','thumb_joint_3']
        preshape.position=[0.0] * len(preshape.name)

        palm_ik = self.get_ik(robot_js, hand_js, [grasp_pose], preshape)
        #self.display_trajectory(palm_ik.robot_joint_traj, '/robot_ik')

        prior_joints = np.zeros((23,))

        for i in range(23):
            prior_joints[i] = palm_ik.robot_joint_traj.points[1].positions[i]

        # Some hard-coded stuff for testing:
        # Make grasp start semi-close to object. Get joint preshape from prior model.
        #prior_joints = np.zeros((23,))
        # prior_joints[1] = 0.9
        # prior_joints[3] = -1.43
        
        # prior_joints[0] = -0.27
        # prior_joints[1] = 1.44
        # prior_joints[3] = -1.28
        # prior_joints[4] = -0.3
        # prior_joints[5] = -0.97
        # prior_joints[6] = 1.95

        # Test shape for closing grasp:
        # prior_joints[0] = -0.15
        # prior_joints[1] = 1.33
        # prior_joints[3] = -1.2
        # prior_joints[5] = -0.91
        # prior_joints[6] = 1.59
        # prior_joints[19] = 1.74
        # prior_joints[21] = -0.45

        for i, preshape_jt in enumerate(self.preshape_joints):
            prior_joints[preshape_jt] = config[6+i]

        print prior_joints
        return prior_joints    

    def visualize_pose(self, pose, frame):
        # TODO: Replace with pose marker, not arrow.
        marker = Marker()
        marker.header.frame_id = frame
        marker.ns = "basic_shapes"
        marker.id = 0
        marker.action = Marker.ADD
        marker.pose = pose
        marker.scale.x = 0.1
        marker.scale.y = 0.01
        marker.scale.z = 0.01
        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 0.0
        marker.color.a = 1.0
        marker.type = Marker.ARROW

        pub = rospy.Publisher('pose_python', Marker)
        rate = rospy.Rate(10)
        for i in range(100):
            pub.publish(marker)
            rate.sleep()
    
    def visualize_obj_cloud(self, obj_cloud, frame):
        marker = Marker()
        marker.header.frame_id = frame
        marker.ns = "basic_shapes"
        marker.id = 0
        marker.action = Marker.ADD
        marker.pose = Pose()
        marker.scale.x = 0.01
        marker.scale.y = 0.01
        marker.scale.z = 0.01
        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 0.0
        marker.color.a = 1.0
        marker.type = Marker.POINTS

        for i in range(len(obj_cloud)):
            p = Point()
            p.x = obj_cloud[i][0]
            p.y = obj_cloud[i][1]
            p.z = obj_cloud[i][2]            
            marker.points.append(p)

        pub = rospy.Publisher('pt_cloud_python', Marker)
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            pub.publish(marker)
            rate.sleep()

    def visualize_cloud(self, obj_cloud, frame, topic='cloud'):
        marker = Marker()
        marker.header.frame_id = frame
        marker.ns = "basic_shapes"
        marker.id = 0
        marker.action = Marker.ADD
        marker.pose = Pose()
        marker.scale.x = 0.01
        marker.scale.y = 0.01
        marker.scale.z = 0.01
        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 0.0
        marker.color.a = 1.0
        marker.type = Marker.POINTS

        for i in range(len(obj_cloud)):
            p = Point()
            p.x = obj_cloud[i][0]
            p.y = obj_cloud[i][1]
            p.z = obj_cloud[i][2]            
            marker.points.append(p)

        pub = rospy.Publisher(topic, Marker)
        rate = rospy.Rate(10)
        #while not rospy.is_shutdown():
        for i in range(10):
            pub.publish(marker)
            rate.sleep()
    
            
    def publish_transform(self, pose, name, base):
        br = tf.TransformBroadcaster()
        rate = rospy.Rate(10.0)
        while not rospy.is_shutdown():
            br.sendTransform((pose.position.x, pose.position.y, pose.position.z),
                             (pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w),
                             rospy.Time.now(),
                             name, base)
            rate.sleep()
    
    # Get grasp info:
    # There are three ways we represent a graspable object:
    # 1. True mesh: pose, mesh file.
    # 2. SDF Model: pose, embedding, scale, size.
    # 3. Grasp success model: pose, partial voxels, size. Note: this will eventually reuse SDF stuff.
    # Generate using Gazebo: load a mesh into Gazebo and segment point cloud.
    def get_grasp_obj_info(self, obj_pt_cloud, mesh_save_file, pt_cld_save_file, grasp_name=None, verbose=False):

        # Client to create SDF embedding.
        embed_client = EmbedClient()

        # Center point cloud in new frame.
        obj_cloud = np.array(list(pc2.read_points(obj_pt_cloud, field_names = ("x", "y", "z"), skip_nans=True)))

        # Save the point cloud to the desired file as npz.
        numpy.savez(pt_cld_save_file, obj_cloud=obj_cloud)

        # Find object frame and convert point cloud to that frame.
        align_transform_matrix, centroid = find_object_frame(obj_cloud, verbose)

        for i in range(len(obj_cloud)):
            obj_cloud[i] = np.dot(align_transform_matrix, [obj_cloud[i][0], obj_cloud[i][1], obj_cloud[i][2], 1.0])[:3]

        # Find width (x), height (y), and depth (z) of the point cloud after alignment.
        width = np.amax(obj_cloud[:,0]) - np.amin(obj_cloud[:,0])
        height = np.amax(obj_cloud[:,1]) - np.amin(obj_cloud[:,1])
        depth = np.amax(obj_cloud[:,2]) - np.amin(obj_cloud[:,2])
        whd = [width, height, depth]

        # Find w/h/d but from the center out - this is used to form a bounding box to speed up PointSDF checks.
        w_from_center = max(abs(np.amax(obj_cloud[:,0])), abs(np.amin(obj_cloud[:,0])))
        h_from_center = max(abs(np.amax(obj_cloud[:,1])), abs(np.amin(obj_cloud[:,1])))
        d_from_center = max(abs(np.amax(obj_cloud[:,2])), abs(np.amin(obj_cloud[:,2])))
        sdf_whd = [w_from_center, h_from_center, d_from_center]

        # Get transform to aligned object frame.
        aligned_object_pose = Pose()
        align_transform_inv = np.linalg.inv(align_transform_matrix)
        pose_quat = tf.transformations.quaternion_from_matrix(align_transform_inv)
        aligned_object_pose.position.x = align_transform_inv[0,3]
        aligned_object_pose.position.y = align_transform_inv[1,3]
        aligned_object_pose.position.z = align_transform_inv[2,3]
        aligned_object_pose.orientation.x = pose_quat[0]
        aligned_object_pose.orientation.y = pose_quat[1]
        aligned_object_pose.orientation.z = pose_quat[2]
        aligned_object_pose.orientation.w = pose_quat[3]

        # Get SDF/Grasp embeddings.
        # Embed the object.
        sdf_embedding, sdf_scale, grasp_embedding = embed_client.embed_point_cloud(obj_cloud, verbose)

        # Create collision mesh to be used in MoveIt!
        # TODO: Parameterize for a bunch of different ways to handle (i.e., nothing, bounding box, partial cloud meshed, reconstruction meshed).
        partial_mesh_file = mesh_save_file
        partial_mesh = 'file://' + partial_mesh_file

        # Build mesh based on requested approach:
        if self.coll_mesh_approach == "sdf":
            embed_client.create_reconstruction_sdf(obj_cloud, partial_mesh_file)
        elif self.coll_mesh_approach == "partial":
            embed_client.create_partial_mesh(obj_cloud, partial_mesh_file)
            
        # Collect SDF representation.
        sdf_embedding = list(np.reshape(sdf_embedding, (-1)))
        grasp_embedding = list(np.reshape(grasp_embedding, (-1)))

        # 5. SDF/Grasp Posterior use the same frame.
        # Now get aligned object pose in the lbr4 base link frame.
        aligned_object_pose_stamped = PoseStamped()
        aligned_object_pose_stamped.header.frame_id = 'world'
        aligned_object_pose_stamped.pose = aligned_object_pose
        aligned_object_pose = self.tf_listener.transformPose('lbr4_base_link', aligned_object_pose_stamped).pose

        # Collect posterior representation.
        grasp_size = whd
        grasp_pose = [aligned_object_pose.position.x, aligned_object_pose.position.y, aligned_object_pose.position.z, aligned_object_pose.orientation.x, aligned_object_pose.orientation.y, aligned_object_pose.orientation.z, aligned_object_pose.orientation.w]
        sdf_pose = grasp_pose
        partial_mesh_pose = grasp_pose

        return sdf_embedding, sdf_scale, sdf_pose, sdf_whd, grasp_embedding, grasp_size, grasp_pose, partial_mesh, partial_mesh_pose

    def get_reconstruction_grasp_plan(self, plan_req):
        # Get true mesh information.
        true_mesh = plan_req.true_mesh
        grasp_name = true_mesh.split('/')[-1].replace('.stl', '') + '_' + str(plan_req.grasp_id)
        # grasp_name = None
        true_pose = plan_req.true_pose # In world pose.

        # Convert pose to the lbr4_base_link frame.
        # true_pose_world = PoseStamped()
        # true_pose_world.pose.position.x, true_pose_world.pose.position.y, \
        #     true_pose_world.pose.position.z = true_pose[:3]
        # true_pose_world.pose.orientation.x, true_pose_world.pose.orientation.y, \
        #     true_pose_world.pose.orientation.z, true_pose_world.pose.orientation.w = true_pose[3:]
        # true_pose_world.header.frame_id = 'world'

        # true_pose_lbr4 = self.tf_listener.transformPose('lbr4_base_link', true_pose_world).pose
        true_pose_lbr4 = Pose()

        #self.visualize_pose(true_pose_lbr4, 'lbr4_base_link')
        true_pose = [true_pose_lbr4.position.x, true_pose_lbr4.position.y, true_pose_lbr4.position.z, true_pose_lbr4.orientation.x, true_pose_lbr4.orientation.y, true_pose_lbr4.orientation.z, true_pose_lbr4.orientation.w]

        # Get embeddings + NN representations from the point cloud.
        sdf_embedding, sdf_scale, sdf_pose, sdf_whd, grasp_embedding, grasp_size, grasp_pose, partial_mesh, partial_mesh_pose = self.get_grasp_obj_info(plan_req.cloud, grasp_name=grasp_name)

        # Cache this information for easy replay + debugging.
        self.write_obj_info_to_yaml('/home/markvandermerwe/catkin_ws/src/ll4ma_scene_description/data/grasp_objs/temp.yaml', sdf_pose, grasp_pose, grasp_size, grasp_embedding, sdf_embedding, sdf_scale, true_mesh, true_pose, sdf_whd, partial_mesh, partial_mesh_pose)

        self.update_grasp_planner(
            sdf_embedding,
            sdf_scale,
            sdf_pose,
            sdf_whd,
            grasp_embedding,
            grasp_size,
            grasp_pose,
            true_mesh,
            true_pose,
            partial_mesh,
            partial_mesh_pose)

        # self.update_ll4ma_planner(
        #     sdf_embedding,
        #     sdf_scale,
        #     sdf_pose,
        #     sdf_whd,
        #     grasp_embedding,
        #     grasp_size,
        #     grasp_pose,
        #     true_mesh,
        #     true_pose,
        #     partial_mesh,
        #     partial_mesh_pose)

        # Sample an initial setting:
        joint_settings = self.sample_grasp_preshape(grasp_pose)
        init_joint_traj = self.get_trajectory_from_joint_angles(joint_settings)
        self.display_trajectory(init_joint_traj, "/seed_traj")
        
        # Get current hand/robot joint states.
        robot_js, hand_js = self.get_joint_states()
        
        # Call grasp_planner to get a grasp configuration.
        print "Calling grasp planner."
        start = time.time()
        palm_plan_response = self.get_grasp_plan(robot_js, hand_js, JointState(), init_joint_traj)
        if palm_plan_response is None:
            response = GetGraspPlanResponse()
            return response
            
        end = time.time()
        print("Planning time: ", (end-start), " sec.")
        print("Planning time: ", ((end-start)/60.), " min.")
        self.display_trajectory(palm_plan_response.robot_traj, '/robot_grasp')

        # Call ll4ma_planner to get full trajectory plan.
        # Palm pose from the planner should be in desired frame already.
        palm_pose = palm_plan_response.palm_pose

        # Convert palm pose to world frame.
        palm_pose_world = PoseStamped()
        palm_pose_world.pose = palm_pose
        palm_pose_world.header.frame_id = 'lbr4_base_link'
        palm_pose = self.tf_listener.transformPose('world', palm_pose_world).pose

        # Full Grasp joint settings.
        grasp_joints = JointState()
        grasp_joints.name = palm_plan_response.robot_traj.joint_names[:7]
        grasp_joints.position = palm_plan_response.robot_traj.points[len(palm_plan_response.hand_joint_traj.points)-1].positions[:7]

        # Hand joint settings.
        hand_preshape = JointState()
        hand_preshape.name = ['index_joint_0', 'index_joint_1', 'index_joint_2', 'index_joint_3', 'middle_joint_0', 'middle_joint_1',
                              'middle_joint_2', 'middle_joint_3', 'ring_joint_0', 'ring_joint_1', 'ring_joint_2', 'ring_joint_3',
                              'thumb_joint_0', 'thumb_joint_1', 'thumb_joint_2', 'thumb_joint_3']
        hand_preshape.position = palm_plan_response.hand_joint_traj.points[len(palm_plan_response.hand_joint_traj.points)-1].positions

        # Motion planning is now done in grasp_client.py (see grasp_pipeline).
        # full_plan = self.get_palm_plan_traj(robot_js, hand_js, palm_pose, hand_preshape, palm_plan_response.robot_traj)
        # self.display_trajectory(full_plan.robot_joint_traj, '/robot_plan')

        response = GetGraspPlanResponse()
        response.hand_joints = hand_preshape
        response.arm_joints = grasp_joints
        response.grasp_pose = palm_pose
        response.grasp_success_probability = palm_plan_response.success_likelihood
        response.time = palm_plan_response.time

        response.collision_mesh = partial_mesh.replace('file://', '')

        # Get collision mesh pose in world frame.
        coll_mesh_pose = PoseStamped()
        coll_mesh_pose.header.frame_id = 'lbr4_base_link'
        coll_mesh_pose.pose.position.x = partial_mesh_pose[0]
        coll_mesh_pose.pose.position.y = partial_mesh_pose[1]
        coll_mesh_pose.pose.position.z = partial_mesh_pose[2]
        coll_mesh_pose.pose.orientation.x = partial_mesh_pose[3]
        coll_mesh_pose.pose.orientation.y = partial_mesh_pose[4]
        coll_mesh_pose.pose.orientation.z = partial_mesh_pose[5]
        coll_mesh_pose.pose.orientation.w = partial_mesh_pose[6]
        
        response.collision_mesh_pose = self.tf_listener.transformPose('world', coll_mesh_pose)
        
        # response.robot_grasp_traj = full_plan.robot_joint_traj
        # response.arm_grasp_traj = full_plan.arm_joint_traj
        # response.hand_grasp_traj = full_plan.hand_joint_traj
        return response

if __name__=='__main__':
    rospy.init_node('reconstruction_grasp_planner')
    
    planner = ReconstructionGraspPlanner()
    get_grasp_srv = rospy.Service('/reconstruction_grasp_planner/get_grasp_plan', GetGraspPlan, planner.get_reconstruction_grasp_plan)
    
    print "Reconstruction grasp planner ready."
    rospy.spin()

        
