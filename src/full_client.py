import numpy as np
import time
import os
import copy
import rospy
import pdb
import tensorflow as tf

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.per_process_gpu_memory_fraction = 0.2

from sensor_msgs.msg import Image, PointCloud2
import sensor_msgs.point_cloud2 as pc2
import math

import sys
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir('ll4ma_3d_reconstruction') + '/src/data_generation')

sys.path.append('/home/markvandermerwe/catkin_ws/src/ll4ma_3d_reconstruction/src/reconstruction_sdf')
from sdf_pointconv_model import get_embedding_model, get_pointconv_model, get_prediction
from visualization import plot_3d_points

from mise import mise_voxel
from slice_objects import get_plane_labels

# from voxelize_point_cloud import point_cloud_to_voxel
# from object_frame import find_object_frame
# from show_voxel import *

_FULL_MODEL_PATH = '/home/markvandermerwe/models/sdf/full_model/'
_SAVE_PATH = '/home/markvandermerwe/ReconstructedMeshes/SDF_Investigate_2/'
_POINT_CLOUD_SIZE = 1000

class SDFClient:
    '''
    Client recieves point cloud and embeds (w/ reconstruction).
    '''

    def __init__(self):

        # Get sdf model.
        get_sdf, _, _ = get_prediction(get_pointconv_model, _FULL_MODEL_PATH)
        
        self.get_sdf = get_sdf

    def run_point_cloud(self, pc2_cloud, verbose=False):
        '''
        Given a pc2 point cloud, get object frame for cloud, voxelize,
        and embed.
        '''

        obj_cloud = np.array(list(pc2.read_points(pc2_cloud, field_names = ("x", "y", "z"), skip_nans=True)))
        print("Obj cloud size: ", obj_cloud.shape[0])

        # Determine scaling size.
        max_dim = max(
            np.amax(obj_cloud[:,0]) - np.amin(obj_cloud[:,0]),
            np.amax(obj_cloud[:,1]) - np.amin(obj_cloud[:,1]),
            np.amax(obj_cloud[:,2]) - np.amin(obj_cloud[:,2]),
        )

        centroid = np.array([
            (np.amax(obj_cloud[:,0]) + np.amin(obj_cloud[:,0])) / 2,
            (np.amax(obj_cloud[:,1]) + np.amin(obj_cloud[:,1])) / 2,
            (np.amax(obj_cloud[:,2]) + np.amin(obj_cloud[:,2])) / 2,
        ])

        # Center.
        obj_cloud[:,0] -= centroid[0]
        obj_cloud[:,1] -= centroid[1]
        obj_cloud[:,2] -= centroid[2]

        # Scale so that max dimension is about 1.
        scale = (1.0/1.03) / max_dim
        print("Scale, ", scale)

        # Scale every point.
        obj_cloud = obj_cloud * scale

        for i in range(30):
            # Down/Up Sample cloud so everything has the same # of points.
            idxs = np.random.choice(obj_cloud.shape[0], size=_POINT_CLOUD_SIZE, replace=True)
            obj_cloud = obj_cloud[idxs,:]

            # Visualize.
            if verbose:
                plot_3d_points(np.reshape(obj_cloud, (-1,3)))

            # Process the point cloud.
            def get_sdf_query(pts):
                return self.get_sdf(obj_cloud, pts)

            get_plane_labels(get_sdf_query, 'x', os.path.join(_SAVE_PATH, 'full_x_' + str(i) + '.png'))
            get_plane_labels(get_sdf_query, 'y', os.path.join(_SAVE_PATH, 'full_y_' + str(i) + '.png'))
            get_plane_labels(get_sdf_query, 'z', os.path.join(_SAVE_PATH, 'full_z_' + str(i) + '.png'))

        # Bounds of 3D space to evaluate in: [-bound, bound] in each dim.
        bound = 0.8
        # Starting voxel resolution.
        initial_voxel_resolution = 32
        # Final voxel resolution.
        final_voxel_resolution = 512
        voxel_size = 0.1
        mise_voxel(get_sdf_query, bound, initial_voxel_resolution, final_voxel_resolution, voxel_size, [0,0,0], '/home/markvandermerwe/ReconstructedMeshes/SDF_Test/full.off')

if __name__ == '__main__':
    f_client = SDFClient()
