#!/usr/bin/env python
import roslib
import rospy
import os
import tf

from geometry_msgs.msg import Pose, Quaternion, PoseStamped, PointStamped

if __name__ == '__main__':

    rospy.init_node('object_frame_broadcaster')

    # Read in grasp object frame.
    grasp_obj_pose = rospy.get_param("/ll4ma_scene/grasp_pose")
    grasp_obj_sdf_pose = rospy.get_param("/ll4ma_scene/sdf_pose")
    #grasp_sdf_quat = tf.transformations.quaternion_from_euler(grasp_obj_sdf_pose[3], grasp_obj_sdf_pose[4], grasp_obj_sdf_pose[5])
    
    br = tf.TransformBroadcaster()
    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        br.sendTransform((grasp_obj_pose[0], grasp_obj_pose[1], grasp_obj_pose[2]),
                         (grasp_obj_pose[3], grasp_obj_pose[4], grasp_obj_pose[5], grasp_obj_pose[6]),
                         rospy.Time.now(),
                         'grasp_obj', 'lbr4_base_link')
        br.sendTransform((grasp_obj_sdf_pose[0], grasp_obj_sdf_pose[1], grasp_obj_sdf_pose[2]),
                         (grasp_obj_sdf_pose[3], grasp_obj_sdf_pose[4], grasp_obj_sdf_pose[5], grasp_obj_sdf_pose[6]),
                         rospy.Time.now(),
                         'sdf_grasp_obj', 'lbr4_base_link')
        rate.sleep()
