#!/usr/bin/env python

import roslib
roslib.load_manifest('reconstruction_grasp_pipeline')
import rospy
import os

from geometry_msgs.msg import Pose, Quaternion, PoseStamped, PointStamped, Point
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sensor_msgs.msg import JointState
from gazebo_msgs.msg import ModelStates
from moveit_msgs.msg import DisplayTrajectory, RobotTrajectory
from visualization_msgs.msg import Marker

from grasp_pipeline.srv import *
from point_cloud_segmentation.srv import *
from ll4ma_planner.srv import *
from grasp_planner.srv import *
from ll4ma_opt_utils.srv import *

import roslib.packages as rp
import sensor_msgs.point_cloud2 as pc2
from sklearn.decomposition import PCA
import tf
import random
import copy
import numpy as np
import time
import pickle

import mcubes

from urdf_parser_py.urdf import URDF
from pykdl_utils.kdl_kinematics import KDLKinematics

from robot_class import robotInterface
sys.path.append(rp.get_pkg_dir('hand_services') 
                + '/scripts')
from hand_client import handClient

sys.path.append(rp.get_pkg_dir('ll4ma_3d_reconstruction') + '/src/data_generation')
sys.path.append(rp.get_pkg_dir('ll4ma_3d_reconstruction') + '/src/reconstruction_voxel')
sys.path.append(rp.get_pkg_dir('prob_grasp_planner') + '/src/grasp_common_library')

from object_frame import find_object_frame
from voxelize_point_cloud import point_cloud_to_voxel
from embed_client import EmbedClient
from full_client import SDFClient

import pdb

class ReconstructionGraspClient:

    def __init__(self):
        # Create a tf listener to handle pose transformations.
        self.tf_listener = tf.TransformListener()

        # Create robot interface to communicate trajectories.
        self.robot_interface = robotInterface(init_node=False)

        # Create hand client.
        self.hand_client = handClient()

        self.object_meshes_path = '/home/markvandermerwe/YCB/ycb_meshes'
        
    def get_pose_stamped_from_array(self, pose_array, frame_id='/world'):
        pose_stamped = PoseStamped()
        pose_stamped.header.frame_id = frame_id 

        pose_quaternion = tf.transformations.quaternion_from_euler(pose_array[0], pose_array[1], pose_array[2])
        pose_stamped.pose.orientation.x, pose_stamped.pose.orientation.y, \
                pose_stamped.pose.orientation.z, pose_stamped.pose.orientation.w = pose_quaternion 
        pose_stamped.pose.position.x, pose_stamped.pose.position.y, pose_stamped.pose.position.z = \
                pose_array[3:]
        return pose_stamped
        
    def insert_object(self, object_name):
        '''
        Insert an object.
        '''
        object_pose_array = [0., 0., 0., -0.1, -0.8, 0.59]

        rospy.loginfo('Waiting for service update_gazebo_object.')
        rospy.wait_for_service('update_gazebo_object')
        rospy.loginfo('Calling service update_gazebo_object.')
        try:
            update_object_gazebo_proxy = rospy.ServiceProxy('update_gazebo_object', UpdateObjectGazebo)
            update_object_gazebo_request = UpdateObjectGazeboRequest()
            update_object_gazebo_request.object_name = object_name
            update_object_gazebo_request.object_pose_array = object_pose_array
            update_object_gazebo_request.object_model_name = object_name
            update_object_gazebo_response = update_object_gazebo_proxy(update_object_gazebo_request) 
            #print update_object_gazebo_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service update_gazebo_object call failed: %s'%e)
        rospy.loginfo('Service update_gazebo_object is executed %s.'%str(update_object_gazebo_response))

        return update_object_gazebo_response.success, object_pose_array

    def remove_objects(self, object_names):
        rospy.loginfo('Waiting for service remove_gazebo_object.')
        rospy.wait_for_service('remove_gazebo_object')
        rospy.loginfo('Calling service remove_gazebo_object.')
        try:
            remove_object_gazebo_proxy = rospy.ServiceProxy('remove_gazebo_object', RemoveObjectGazebo)
            remove_object_gazebo_request = RemoveObjectGazeboRequest()
            remove_object_gazebo_request.object_names = object_names
            remove_object_gazebo_response = remove_object_gazebo_proxy(remove_object_gazebo_request)
        except rospy.ServiceException, e:
            rospy.loginfo('Service remove_gazebo_object call failed: %s'%e)
        rospy.loginfo('Service remove_gazebo_object is executed %s.'%str(remove_object_gazebo_response))
        return remove_object_gazebo_response.success

    def remove_objects(self, object_names):
        rospy.loginfo('Waiting for service remove_gazebo_object.')
        rospy.wait_for_service('remove_gazebo_object')
        rospy.loginfo('Calling service remove_gazebo_object.')
        try:
            remove_object_gazebo_proxy = rospy.ServiceProxy('remove_gazebo_object', RemoveObjectGazebo)
            remove_object_gazebo_request = RemoveObjectGazeboRequest()
            remove_object_gazebo_request.object_names = object_names
            remove_object_gazebo_response = remove_object_gazebo_proxy(remove_object_gazebo_request)
        except rospy.ServiceException, e:
            rospy.loginfo('Service remove_gazebo_object call failed: %s'%e)
        rospy.loginfo('Service remove_gazebo_object is executed %s.'%str(remove_object_gazebo_response))
        return remove_object_gazebo_response.success

    def segment_object_client(self):
        rospy.loginfo('Waiting for service object_segmenter.')
        rospy.wait_for_service('object_segmenter')
        rospy.loginfo('Calling service object_segmenter.')
        try:
            object_segment_proxy = rospy.ServiceProxy('object_segmenter', SegmentGraspObject)
            object_segment_request = SegmentGraspObjectRequest()
            object_segment_response = object_segment_proxy(object_segment_request) 
            #print self.object_segment_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service object_segmenter call failed: %s'%e)
            object_segment_response = None
        rospy.loginfo('Service object_segmenter is executed.')
        return object_segment_response

    def hand_grasp_control(self):
        joint_idx = [1, 2, 3, 5, 6, 7, 9, 10, 11, 14, 15]
        self.hand_client.grasp_object(joint_idx)
        cur_js = self.hand_client.get_joint_state() 
        increase_stiffness_times = 3
        for i in xrange(increase_stiffness_times):
            _, cur_js = self.hand_client.increase_stiffness(joint_idx, cur_js)

    def lift_task_vel_planner_client(self, height_to_lift=0.15):
        rospy.loginfo('Waiting for service straight_line_planner to lift.')
        rospy.wait_for_service('straight_line_planner')
        rospy.loginfo('Calling service straight_line_planner to lift.')
        try:
            planning_proxy = rospy.ServiceProxy('straight_line_planner', StraightLinePlan)
            planning_request = StraightLinePlanRequest()
            planning_request.lift_height = height_to_lift
            response = planning_proxy(planning_request) 
        except rospy.ServiceException, e:
            rospy.loginfo('Service straight_line_planner call to lift failed: %s'%e)
            rospy.loginfo('Service straight_line_planner to lift is executed %s.'
                          %str(response.success))
        return response.plan_traj

    def execute_arm_plan(self, plan_traj, send_cmd_manually=False):

        # Smooth trajectory.
        plan_traj = self.robot_interface.get_smooth_traj(plan_traj)

        # TODO: Check for success in the plan.

        if send_cmd_manually:
            #self.palm_planner.plot_traj(plan_traj)

            # send to robot:
            send_cmd = raw_input('send to robot? (y/n)')
            if(send_cmd == 'y'):
                self.robot_interface.send_jtraj(plan_traj)
                #TO DO: make sure the robot finishing executing the trajectory before returning.
                return True
            return False
        else:
            # send to robot:
            self.robot_interface.send_jtraj(plan_traj)
            return True

    def move_arm_home(self):
        rospy.loginfo('Move the arm to go home.')
        robot_js, hand_js = self.get_joint_states()
        hand_final_shape = copy.deepcopy(hand_js)

        home_pose = Pose()
        home_pose.position.x = -0.020804
        home_pose.position.y = -0.001498
        home_pose.position.z = 1.33069
        home_pose.orientation.x = -0.007979
        home_pose.orientation.y = 0.0
        home_pose.orientation.z = 1.0
        home_pose.orientation.w = 0.0

        arm_traj = self.get_palm_plan_traj(robot_js, hand_js, home_pose, hand_final_shape)
        self.execute_arm_plan(arm_traj, send_cmd_manually=False)

    def display_trajectory(self, trajectory, pub_name):
        # Display the final grasp plan.
        pub = rospy.Publisher(pub_name, DisplayTrajectory, queue_size=1)
        traj_ = DisplayTrajectory()
        traj_.model_id= 'lbr4'
        robot_traj_ = RobotTrajectory()
        robot_traj_.joint_trajectory = trajectory
        traj_.trajectory.append(robot_traj_)
    
        # while True:
        for i in range(100000):
            pub.publish(traj_)

    def get_trajectory_from_joint_angles(self, joint_angles):
        new_traj = JointTrajectory()
        new_traj.header.frame_id = "/lbr4_base_frame"
        new_traj.joint_names = self.joint_names

        new_pt = JointTrajectoryPoint()
        new_pt.positions = joint_angles

        new_traj.points.append(new_pt)
        return new_traj
                    
    def perform_grasp(self, robot_js, hand_js, palm_pose, hand_preshape):
        '''
        Do motion planning for given object + grasp preshape using
        trajectory optimization (ee_pose_problem in ll4ma_planner).
        '''

        # Get arm trajectory.
        arm_traj = self.get_palm_plan_traj(robot_js, hand_js, palm_pose, hand_preshape)

        # Move hand to preshape
        # self.control_allegro_config_client(hand_preshape)

        # # Send arm trajectory.
        # x = raw_input("Send plan? y/n")
        # if x == 'y':
        #     self.execute_arm_plan(arm_traj, send_cmd_manually=True)

        #     # Close hand.
        #     raw_input("close hand?")
        #     self.hand_grasp_control()

        #     # Lift.
        #     raw_input("lift?")
        #     lift_traj = self.lift_task_vel_planner_client()
        #     self.execute_arm_plan(lift_traj, send_cmd_manually=True)

        #     # Drop.
        #     raw_input("drop?")
        #     self.control_allegro_config_client(hand_preshape)
            
        #     # Reset.
        #     raw_input("go home?")
        #     self.move_arm_home()

        return arm_traj

    def visualize_pose(self, pose, frame):
        marker = Marker()
        marker.header.frame_id = frame
        marker.ns = "basic_shapes"
        marker.id = 0
        marker.action = Marker.ADD
        marker.pose = pose
        marker.scale.x = 0.1
        marker.scale.y = 0.01
        marker.scale.z = 0.01
        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 0.0
        marker.color.a = 1.0
        marker.type = Marker.ARROW

        pub = rospy.Publisher('pose_python', Marker)
        rate = rospy.Rate(10)
        for i in range(10):
            pub.publish(marker)
            rate.sleep()
    
    def visualize_obj_cloud(self, obj_cloud, frame):
        marker = Marker()
        marker.header.frame_id = frame
        marker.ns = "basic_shapes"
        marker.id = 0
        marker.action = Marker.ADD
        marker.pose = Pose()
        marker.scale.x = 0.01
        marker.scale.y = 0.01
        marker.scale.z = 0.01
        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 0.0
        marker.color.a = 1.0
        marker.type = Marker.POINTS

        for i in range(len(obj_cloud)):
            p = Point()
            p.x = obj_cloud[i][0]
            p.y = obj_cloud[i][1]
            p.z = obj_cloud[i][2]            
            marker.points.append(p)

        pub = rospy.Publisher('pt_cloud_python', Marker)
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            pub.publish(marker)
            rate.sleep()
    
    def publish_transform(self, pose, name, base):
        br = tf.TransformBroadcaster()
        rate = rospy.Rate(10.0)
        while not rospy.is_shutdown():
            br.sendTransform((pose.position.x, pose.position.y, pose.position.z),
                             (pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w),
                             rospy.Time.now(),
                             name, base)
            rate.sleep()

    def get_grasp_plan(self, obj_cloud, true_mesh, true_pose):
        rospy.loginfo('Waiting for service /reconstruction_grasp_planner/get_grasp_plan')
        rospy.wait_for_service('/reconstruction_grasp_planner/get_grasp_plan')
        rospy.loginfo('Calling /reconstruction_grasp_planner/get_grasp_plan service.')

        try:
            get_grasp_plan_proxy = rospy.ServiceProxy('/reconstruction_grasp_planner/get_grasp_plan', GetGraspPlan)

            get_grasp_request = GetGraspPlanRequest()
            get_grasp_request.cloud = obj_cloud
            get_grasp_request.true_mesh = true_mesh
            get_grasp_request.true_pose = true_pose
            get_grasp_response = get_grasp_plan_proxy(get_grasp_request)
        except rospy.ServiceException, e:
            rospy.loginfo('Service get_grasp_plan call failed: %s'%e)
            return None
        return get_grasp_response
            
if __name__ == '__main__':

    rospy.init_node('reconstruction_grasp_client')

    load_cached_obj = rospy.get_param("use_obj_file")
    
    # Make client to perform the grasps.
    grasp_client = ReconstructionGraspClient()
    
    # Object dataset (YCB)
    dataset_dir = '/home/markvandermerwe/YCB/ycb'
    object_mesh_dirs = os.listdir(dataset_dir)
    #random.shuffle(object_mesh_dirs)
    object_mesh_dirs = ['004_sugar_box', '036_wood_block']

    for i, object_name in enumerate(object_mesh_dirs):
        rospy.loginfo('Object: %s' %object_name)

        insert_success, object_pose = grasp_client.insert_object(object_name)
        if not insert_success:
            print("Problem inserting object. Continuing.")
            continue

        true_pose = grasp_client.get_pose_stamped_from_array(object_pose).pose
        # true_pose = grasp_client.tf_listener.transformPose('lbr4_base_link', true_pose).pose
        true_pose = [true_pose.position.x, true_pose.position.y, true_pose.position.z, true_pose.orientation.x, true_pose.orientation.y, true_pose.orientation.z, true_pose.orientation.w]
        # Get mesh file.
        true_mesh = 'file:///home/markvandermerwe/catkin_ws/src/ll4ma_scene_description/data/meshes/' + object_name + '.stl'

        # Get GraspObject.
        grasp_obj = None
        # Segment object in scene.
        object_segment_response = grasp_client.segment_object_client()
        retr = None
        while object_segment_response is None:
            retr = raw_input("retry? y/n")
            if retr == 'y':
                object_segment_response = grasp_client.segment_object_client()
            else:
                break
        if retr is not None and retr != 'y':
            print("Problem segmenting object. Continuing.")
            continue
        grasp_obj = object_segment_response.obj

        # Get grasp plan.
        grasp_plan_response = grasp_client.get_grasp_plan(grasp_obj.cloud, true_mesh, true_pose)

        # Remove the object from the scene.
        #grasp_client.remove_objects([object_name])

        raw_input("Wait.")        
        
