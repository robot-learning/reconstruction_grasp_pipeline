#!/usr/bin/env python

import roslib
roslib.load_manifest('reconstruction_grasp_pipeline')
import rospy
import os

from geometry_msgs.msg import Pose, Quaternion, PoseStamped, PointStamped
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sensor_msgs.msg import JointState
from gazebo_msgs.msg import ModelStates
from moveit_msgs.msg import DisplayTrajectory, RobotTrajectory

from reconstruction_grasp_pipeline.srv import *
from point_cloud_segmentation.srv import *
from grasp_data_collection_pkg.srv import *
from ll4ma_planner.srv import *
from grasp_planner.srv import *
from ll4ma_opt_utils.srv import *

import roslib.packages as rp
import sensor_msgs.point_cloud2 as pc2
from sklearn.decomposition import PCA
import tf
import random
import copy
import numpy as np
import time
import pickle

import mcubes

from urdf_parser_py.urdf import URDF
from pykdl_utils.kdl_kinematics import KDLKinematics

from robot_class import robotInterface
sys.path.append(rp.get_pkg_dir('hand_services') 
                + '/scripts')
from hand_client import handClient

sys.path.append(rp.get_pkg_dir('ll4ma_3d_reconstruction') + '/src/data_generation')
sys.path.append(rp.get_pkg_dir('ll4ma_3d_reconstruction') + '/src/reconstruction_voxel')
sys.path.append(rp.get_pkg_dir('prob_grasp_planner') + '/src/grasp_common_library')

from align_object_frame import align_object
from voxelize_point_cloud import point_cloud_to_voxel
from embed_client import EmbedClient
from full_client import SDFClient

import pdb

class ReconstructionGraspClient:

    def __init__(self):
        # Create a tf listener to handle pose transformations.
        self.tf_listener = tf.TransformListener()

        # Create robot interface to communicate trajectories.
        self.robot_interface = robotInterface(init_node=False)

        # Create hand client.
        self.hand_client = handClient()

        self.object_meshes_path = '/home/markvandermerwe/YCB/ycb_meshes'

        # Qingkai's grasp prior model. Used to initialize optimization.
        prior_model_path = '/home/markvandermerwe/models/Qingkai/models/grasp_al_prior/prior_2_sets'
        self.prior_model = pickle.load(open(prior_model_path, 'rb'))

        # Setup KDL to do inverse kinematics.
        # self.robot = URDF.from_parameter_server()
        # base_link = 'lbr4_base_link'
        # end_link = 'palm_link'
        # self.robot_kdl = KDLKinematics(self.robot, base_link, end_link)

        # Hand joint angles we care about.
        self.preshape_joints = rospy.get_param("ll4ma_planner/preshape_joints", [])
        self.joint_names = rospy.get_param("ll4ma_planner/joint_names", [])

    def write_obj_info_to_yaml(self, filename, obj_pose_sdf, obj_pose, obj_size, partial_voxel, sdf_embedding, scale, mesh_file, mesh_pose):
        '''
        Create a yaml file for this object and write all the info to it.
        '''
        with open(filename, 'w') as f:
            f.write("ll4ma_scene:\n")

            f.write("  grasp_obj_pose_sdf: " + str(obj_pose_sdf) + "\n")            
            f.write("  grasp_obj_pose: " + str(obj_pose) + "\n")
            f.write("  grasp_obj_size: " + str(obj_size) + "\n")
            f.write("  grasp_obj_scale: " + str(scale) + "\n")
            f.write("  grasp_obj_mesh: \"" + str(mesh_file) + "\"\n")
            f.write("  grasp_mesh_pose: " + str(mesh_pose) + "\n")
            f.write("  grasp_embedding: " + str(sdf_embedding) + "\n")

            f.write("  grasp_partial_voxel: [")
            for vox in list(partial_voxel):
                f.write(str(float(vox)) + ", ")
            f.write("]\n")
        
    def get_pose_stamped_from_array(self, pose_array, frame_id='/world'):
        pose_stamped = PoseStamped()
        pose_stamped.header.frame_id = frame_id 

        pose_quaternion = tf.transformations.quaternion_from_euler(pose_array[0], pose_array[1], pose_array[2])
        pose_stamped.pose.orientation.x, pose_stamped.pose.orientation.y, \
                pose_stamped.pose.orientation.z, pose_stamped.pose.orientation.w = pose_quaternion 
        pose_stamped.pose.position.x, pose_stamped.pose.position.y, pose_stamped.pose.position.z = \
                pose_array[3:]
        return pose_stamped
        
    def insert_object(self, object_name):
        '''
        Insert an object.
        '''
        object_pose_array = [0., 0., 0., 0., -0.8, 0.59]

        rospy.loginfo('Waiting for service update_gazebo_object.')
        rospy.wait_for_service('update_gazebo_object')
        rospy.loginfo('Calling service update_gazebo_object.')
        try:
            update_object_gazebo_proxy = rospy.ServiceProxy('update_gazebo_object', UpdateObjectGazebo)
            update_object_gazebo_request = UpdateObjectGazeboRequest()
            update_object_gazebo_request.object_name = object_name
            update_object_gazebo_request.object_pose_array = object_pose_array
            update_object_gazebo_request.object_model_name = object_name
            update_object_gazebo_response = update_object_gazebo_proxy(update_object_gazebo_request) 
            #print update_object_gazebo_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service update_gazebo_object call failed: %s'%e)
        rospy.loginfo('Service update_gazebo_object is executed %s.'%str(update_object_gazebo_response))

        return update_object_gazebo_response.success, object_pose_array

    def remove_objects(self, object_names):
        rospy.loginfo('Waiting for service remove_gazebo_object.')
        rospy.wait_for_service('remove_gazebo_object')
        rospy.loginfo('Calling service remove_gazebo_object.')
        try:
            remove_object_gazebo_proxy = rospy.ServiceProxy('remove_gazebo_object', RemoveObjectGazebo)
            remove_object_gazebo_request = RemoveObjectGazeboRequest()
            remove_object_gazebo_request.object_names = object_names
            remove_object_gazebo_response = remove_object_gazebo_proxy(remove_object_gazebo_request)
        except rospy.ServiceException, e:
            rospy.loginfo('Service remove_gazebo_object call failed: %s'%e)
        rospy.loginfo('Service remove_gazebo_object is executed %s.'%str(remove_object_gazebo_response))
        return remove_object_gazebo_response.success

    def add_object_grasp_planner(self, object_mesh_file, object_pose_lbr4):

        quaternion = [
            object_pose_lbr4.orientation.x,
            object_pose_lbr4.orientation.y,
            object_pose_lbr4.orientation.z,
            object_pose_lbr4.orientation.w,            
        ]
        euler = tf.transformations.euler_from_quaternion(quaternion)
        
        ll4ma_planner_pose = [
            object_pose_lbr4.position.x,
            object_pose_lbr4.position.y,
            object_pose_lbr4.position.z,
            euler[0],
            euler[1],
            euler[2],
        ]
        print ll4ma_planner_pose
        return self.update_grasp_planner(object_mesh_file, ll4ma_planner_pose)

    def remove_objects(self, object_names):
        rospy.loginfo('Waiting for service remove_gazebo_object.')
        rospy.wait_for_service('remove_gazebo_object')
        rospy.loginfo('Calling service remove_gazebo_object.')
        try:
            remove_object_gazebo_proxy = rospy.ServiceProxy('remove_gazebo_object', RemoveObjectGazebo)
            remove_object_gazebo_request = RemoveObjectGazeboRequest()
            remove_object_gazebo_request.object_names = object_names
            remove_object_gazebo_response = remove_object_gazebo_proxy(remove_object_gazebo_request)
        except rospy.ServiceException, e:
            rospy.loginfo('Service remove_gazebo_object call failed: %s'%e)
        rospy.loginfo('Service remove_gazebo_object is executed %s.'%str(remove_object_gazebo_response))
        return remove_object_gazebo_response.success

    def update_ll4ma_planner(self, sdf_embedding, sdf_scale, sdf_pose, grasp_embedding, grasp_size, grasp_pose, true_mesh, true_pose):
        rospy.loginfo('Waiting for service /ll4ma_planner/add_object')
        rospy.wait_for_service('/ll4ma_planner/add_object')
        rospy.loginfo('Calling add_object service.')

        try:
            add_object_proxy = rospy.ServiceProxy('/ll4ma_planner/add_object', AddObject)

            add_object_request = AddObjectRequest()
            add_object_request.sdf_embedding = sdf_embedding
            add_object_request.sdf_scale = sdf_scale
            add_object_request.sdf_pose = sdf_pose
            add_object_request.grasp_embedding = grasp_embedding
            add_object_request.grasp_size = grasp_size
            add_object_request.grasp_pose = grasp_pose
            add_object_request.true_mesh = true_mesh
            add_object_request.true_pose = true_pose
            add_object_response = add_object_proxy(add_object_request)
        except rospy.ServiceException, e:
            rospy.loginfo('Service add_object call failed: %s'%e)
        rospy.loginfo('Service add_object is executed %s.'%str(add_object_response))
        
        return add_object_response.success
    
    def update_grasp_planner(self, cloud_embedding, scale, pose, voxels, obj_size, grasp_pose, obj_mesh_file, mesh_pose):

        rospy.loginfo('Waiting for service /ll4ma_planner/add_object')
        rospy.wait_for_service('/ll4ma_planner/add_object')
        rospy.loginfo('Calling add_object service.')

        try:
            add_object_proxy = rospy.ServiceProxy('/ll4ma_planner/add_object', AddObject)

            add_object_request = AddObjectRequest()
            add_object_request.cloud_embedding = cloud_embedding
            add_object_request.scale = scale
            add_object_request.obj_pose = pose
            add_object_request.voxels = voxels
            add_object_request.obj_size = obj_size
            add_object_request.grasp_obj_pose = grasp_pose
            add_object_request.obj_mesh_file = obj_mesh_file
            add_object_request.mesh_pose = mesh_pose
            add_object_response = add_object_proxy(add_object_request)
        except rospy.ServiceException, e:
            rospy.loginfo('Service add_object call failed: %s'%e)
        rospy.loginfo('Service add_object is executed %s.'%str(add_object_response))

        # rospy.loginfo('Waiting for service /grasp_planner/add_object')
        # rospy.wait_for_service('/grasp_planner/add_object')
        # rospy.loginfo('Calling add_object service.')

        # try:
        #     add_object_proxy = rospy.ServiceProxy('/grasp_planner/add_object', AddObject)

        #     add_object_request = AddObjectRequest()
        #     add_object_request.obj_mesh_file = full_mesh_file
        #     add_object_request.obj_pose = pose

        #     add_object_response = add_object_proxy(add_object_request)
        # except rospy.ServiceException, e:
        #     rospy.loginfo('Service add_object call failed: %s'%e)
        # rospy.loginfo('Service add_object is executed %s.'%str(add_object_response))
        
        return add_object_response.success

    def get_joint_states(self):
        '''
        Return the current arm and hand position.
        '''
        current_joint_state = rospy.wait_for_message('/lbr4_allegro/joint_states', JointState)

        robot_js=JointState()
        robot_js.name=['lbr4_j0','lbr4_j1','lbr4_j2','lbr4_j3','lbr4_j4','lbr4_j5','lbr4_j6']
        robot_js.position=current_joint_state.position[0:7]

        hand_js=JointState()
        hand_js.name=['index_joint_0','index_joint_1','index_joint_2','index_joint_3',
                      'middle_joint_0','middle_joint_1', 'middle_joint_2','middle_joint_3',
                      'ring_joint_0', 'ring_joint_1', 'ring_joint_2', 'ring_joint_3',
                      'thumb_joint_0', 'thumb_joint_1', 'thumb_joint_2','thumb_joint_3']
        hand_js.position=current_joint_state.position[7:]

        return robot_js, hand_js

    def get_table_pose(self):
        '''
        Get pose of the table in the world frame.
        '''
        print "Waiting for model state"
        current_table_pose = rospy.wait_for_message('/gazebo/model_states', ModelStates)

        current_table_pose_stamped = PoseStamped()
        current_table_pose_stamped.pose = current_table_pose.pose[1]
        current_table_pose_stamped.header.frame_id = 'world'

        table_pose_lbr4 = self.tf_listener.transformPose('lbr4_base_link', current_table_pose_stamped).pose

        quaternion = [
            table_pose_lbr4.orientation.x,
            table_pose_lbr4.orientation.y,
            table_pose_lbr4.orientation.z,
            table_pose_lbr4.orientation.w,            
        ]
        euler = tf.transformations.euler_from_quaternion(quaternion)
        
        return [
            table_pose_lbr4.position.x,
            table_pose_lbr4.position.y,
            table_pose_lbr4.position.z,
            euler[0],
            euler[1],
            euler[2],
        ]

    def gen_grasp_preshape_client(self, object_segment_response):
        rospy.loginfo('Waiting for service gen_grasp_preshape.')
        rospy.wait_for_service('gen_grasp_preshape')
        rospy.loginfo('Calling service gen_grasp_preshape.')
        try:
            preshape_proxy = rospy.ServiceProxy('gen_grasp_preshape', GraspPreshape)
            preshape_request = GraspPreshapeRequest()
            #if not self.use_sim:
            #    preshape_request.obj = self.object_segment_response.obj
            #else:
                #preshape_request.obj = self.object_segment_blensor_response.obj
            preshape_request.obj = object_segment_response.obj

            # Testing in simulation
            #preshape_req.obj = self.object_segment_response.obj
            preshape_response = preshape_proxy(preshape_request) 
            #self.preshape_palm_goal_pose_sd_pcd = self.listener.transformPose(self.kinect2_sd_frame_id, 
            #                                    self.preshape_response.palm_goal_pose_in_pcd)
            #print self.preshape_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service gen_grasp_preshape call failed: %s'%e)
            return None
        rospy.loginfo('Service gen_grasp_preshape is executed.')
        return preshape_response

    def segment_object_client(self):
        rospy.loginfo('Waiting for service object_segmenter.')
        rospy.wait_for_service('object_segmenter')
        rospy.loginfo('Calling service object_segmenter.')
        try:
            object_segment_proxy = rospy.ServiceProxy('object_segmenter', SegmentGraspObject)
            object_segment_request = SegmentGraspObjectRequest()
            object_segment_response = object_segment_proxy(object_segment_request) 
            #print self.object_segment_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service object_segmenter call failed: %s'%e)
            object_segment_response = None
        rospy.loginfo('Service object_segmenter is executed.')
        return object_segment_response

    def control_allegro_config_client(self, allegro_joint_state):
        rospy.loginfo('Waiting for service control_allegro_config.')
        rospy.wait_for_service('control_allegro_config')
        rospy.loginfo('Calling service control_allegro_config.')
        try:
            control_proxy = rospy.ServiceProxy('control_allegro_config', AllegroConfig)
            control_request = AllegroConfigRequest()
            control_request.allegro_target_joint_state = allegro_joint_state
            control_response = control_proxy(control_request) 
        except rospy.ServiceException, e:
            rospy.loginfo('Service control_allegro_config call failed: %s'%e)
        rospy.loginfo('Service control_allegro_config is executed %s.'%str(control_response))
        return control_response.success

    def get_grasp_plan(self, arm_j0, hand_j0, hand_shape, init_traj=JointTrajectory()):
        '''
        Run grasp planner.
        '''
        rospy.loginfo('Waiting for service grasp_planner/get_grasp_plan.')
        rospy.wait_for_service('/grasp_planner/get_grasp_plan')
        rospy.loginfo('Calling service. /grasp_planner/get_grasp_plan')
        try:
            palm_plan_proxy = rospy.ServiceProxy('/grasp_planner/get_grasp_plan', FtipGrasp)
            palm_plan_request = FtipGraspRequest()
            palm_plan_request.arm_jstate = arm_j0
            palm_plan_request.hand_jstate = hand_j0
            palm_plan_request.hand_preshape = hand_shape
            palm_plan_request.t_steps = 1 # Just need final pose, rest is planned by the ll4ma_planner.
            palm_plan_request.optimize = True
            palm_plan_request.initial_jtraj = init_traj
            palm_plan_response = palm_plan_proxy(palm_plan_request) 
        except rospy.ServiceException, e:
            rospy.loginfo('Service /grasp_planner/get_grasp_plan call failed: %s'%e)
            return None
        return palm_plan_response
    
    # Stolen from: grasp_planner/scripts/grasp_client.py.
    def get_palm_plan(self, arm_j0,hand_j0,palm_poses,hand_shape,t_steps=1,object_pose=Pose(),init_traj=JointTrajectory()):
        # get plan from service:
        rospy.loginfo('waiting for ll4ma planner')
        #print "waiting for call"
        rospy.wait_for_service('/ll4ma_planner/get_trajectory')
        rospy.loginfo('calling ll4ma planner')
        planner=rospy.ServiceProxy('/ll4ma_planner/get_trajectory', GetEEPlan)
        get_plan_request = GetEEPlanRequest()
        get_plan_request.arm_jstate = arm_j0
        get_plan_request.hand_jstate = hand_j0
        get_plan_request.t_steps = t_steps
        get_plan_request.des_ee_pose = palm_poses
        get_plan_request.object_pose = object_pose
        get_plan_request.ee_preshape = hand_shape
        get_plan_request.initial_jtraj = init_traj
        get_plan_request.optimize = True
        resp=planner(get_plan_request)
        return resp

    def get_palm_plan_traj(self, robot_js, hand_js, palm_pose, hand_preshape, init_traj):
        '''
        Given joints for robot, hand, palm pose, and hand preshape, run and 
        generate collision free trajectories.
        '''
        #resp = self.get_palm_plan(robot_js, hand_js, [palm_pose], hand_preshape)
        resp = self.get_palm_plan(robot_js, hand_js, [palm_pose], hand_preshape, t_steps=10, init_traj=init_traj)

        # TODO: Check result for collisions, etc.

        return resp

    def hand_grasp_control(self):
        joint_idx = [1, 2, 3, 5, 6, 7, 9, 10, 11, 14, 15]
        self.hand_client.grasp_object(joint_idx)
        cur_js = self.hand_client.get_joint_state() 
        increase_stiffness_times = 3
        for i in xrange(increase_stiffness_times):
            _, cur_js = self.hand_client.increase_stiffness(joint_idx, cur_js)

    def lift_task_vel_planner_client(self, height_to_lift=0.15):
        rospy.loginfo('Waiting for service straight_line_planner to lift.')
        rospy.wait_for_service('straight_line_planner')
        rospy.loginfo('Calling service straight_line_planner to lift.')
        try:
            planning_proxy = rospy.ServiceProxy('straight_line_planner', StraightLinePlan)
            planning_request = StraightLinePlanRequest()
            planning_request.lift_height = height_to_lift
            response = planning_proxy(planning_request) 
        except rospy.ServiceException, e:
            rospy.loginfo('Service straight_line_planner call to lift failed: %s'%e)
            rospy.loginfo('Service straight_line_planner to lift is executed %s.'
                          %str(response.success))
        return response.plan_traj

    def execute_arm_plan(self, plan_traj, send_cmd_manually=False):

        # Smooth trajectory.
        plan_traj = self.robot_interface.get_smooth_traj(plan_traj)

        # TODO: Check for success in the plan.

        if send_cmd_manually:
            #self.palm_planner.plot_traj(plan_traj)

            # send to robot:
            send_cmd = raw_input('send to robot? (y/n)')
            if(send_cmd == 'y'):
                self.robot_interface.send_jtraj(plan_traj)
                #TO DO: make sure the robot finishing executing the trajectory before returning.
                return True
            return False
        else:
            # send to robot:
            self.robot_interface.send_jtraj(plan_traj)
            return True

    def move_arm_home(self):
        rospy.loginfo('Move the arm to go home.')
        robot_js, hand_js = self.get_joint_states()
        hand_final_shape = copy.deepcopy(hand_js)

        home_pose = Pose()
        home_pose.position.x = -0.020804
        home_pose.position.y = -0.001498
        home_pose.position.z = 1.33069
        home_pose.orientation.x = -0.007979
        home_pose.orientation.y = 0.0
        home_pose.orientation.z = 1.0
        home_pose.orientation.w = 0.0

        arm_traj = self.get_palm_plan_traj(robot_js, hand_js, home_pose, hand_final_shape)
        self.execute_arm_plan(arm_traj, send_cmd_manually=False)

    def display_trajectory(self, trajectory, pub_name):
        # Display the final grasp plan.
        pub = rospy.Publisher(pub_name, DisplayTrajectory, queue_size=1)
        traj_ = DisplayTrajectory()
        traj_.model_id= 'lbr4'
        robot_traj_ = RobotTrajectory()
        robot_traj_.joint_trajectory = trajectory
        traj_.trajectory.append(robot_traj_)
    
        # while True:
        for i in range(100000):        
            pub.publish(traj_)

    def get_trajectory_from_joint_angles(self, joint_angles):
        new_traj = JointTrajectory()
        new_traj.header.frame_id = "/lbr4_base_link"
        new_traj.joint_names = self.joint_names

        for i in range(len(joint_angles)):
            new_pt = JointTrajectoryPoint()
            new_pt.positions = joint_angles[i]
            new_traj.points.append(new_pt)
            
        return new_traj
            
    def sample_grasp_config(self):
        '''
        Get a grasp preshape config from Qingkai's prior model to seed grasp optimization.
        '''
        self.prior_model.random_state = 42
        # if self.prior_model.random_state != None:
        #     self.prior_model.random_state = None
        return self.prior_model.sample()

    def sample_grasp_preshape(self):
        '''
        Sample a configuration and convert to a full preshape via pykdl.
        '''
        config = self.sample_grasp_config()[0][0]

        # Convert object pose to transformation from object to lbr4 base.
        # obj_lbr4_transform = tf.transformations.quaternion_matrix([object_pose.orientation.x, object_pose.orientation.y, object_pose.orientation.z, object_pose.orientation.w])
        # obj_lbr4_transform[0,3] = object_pose.position.x
        # obj_lbr4_transform[1,3] = object_pose.position.y
        # obj_lbr4_transform[2,3] = object_pose.position.z

        # # Get transformation from grasp to object.
        # grasp_obj_transform = tf.transformations.euler_matrix(config[3], config[4], config[5])
        # grasp_obj_transform[0,3] = config[0]
        # grasp_obj_transform[1,3] = config[1]
        # grasp_obj_transform[2,3] = config[2]

        # # Combine transformations to get grasp pose in lbr4 frame.
        # grasp_lbr4_transform = np.dot(obj_lbr4_transform, grasp_obj_transform)

        # # Pull out grasp pose.
        # grasp_pose = Pose()
        # grasp_pose.position.x = grasp_lbr4_transform[0,3]
        # grasp_pose.position.y = grasp_lbr4_transform[1,3]
        # grasp_pose.position.z = grasp_lbr4_transform[2,3]
        # grasp_pose.orientation.x, grasp_pose.orientation.y, grasp_pose.orientation.z, grasp_pose.orientation.w = tf.transformations.quaternion_from_matrix(grasp_lbr4_transform)

        # Get IK.
        # guess_joints = [0.0, 1.44, 0.0, 0.0, 0.0, 0.0, 0.0]
        # start_joints = self.robot_kdl.inverse(grasp_pose, guess_joints)

        # Make grasp start semi-close to object. Get joint preshape from prior model.
        prior_joints = np.zeros((23,))
        # prior_joints[1] = 0.9
        # prior_joints[3] = -1.43
        prior_joints[0] = -0.27
        prior_joints[1] = 1.44
        prior_joints[3] = -1.28
        prior_joints[4] = -0.3
        prior_joints[5] = -0.97
        prior_joints[6] = 1.95

        for i, preshape_jt in enumerate(self.preshape_joints):
            prior_joints[preshape_jt] = config[6+i]

        print prior_joints
        return prior_joints
        
    def perform_grasp(self, robot_js, hand_js, palm_pose, hand_preshape):
        '''
        Do motion planning for given object + grasp preshape using
        trajectory optimization (ee_pose_problem in ll4ma_planner).
        '''

        # Get arm trajectory.
        arm_traj = self.get_palm_plan_traj(robot_js, hand_js, palm_pose, hand_preshape)

        # Move hand to preshape
        # self.control_allegro_config_client(hand_preshape)

        # # Send arm trajectory.
        # x = raw_input("Send plan? y/n")
        # if x == 'y':
        #     self.execute_arm_plan(arm_traj, send_cmd_manually=True)

        #     # Close hand.
        #     raw_input("close hand?")
        #     self.hand_grasp_control()

        #     # Lift.
        #     raw_input("lift?")
        #     lift_traj = self.lift_task_vel_planner_client()
        #     self.execute_arm_plan(lift_traj, send_cmd_manually=True)

        #     # Drop.
        #     raw_input("drop?")
        #     self.control_allegro_config_client(hand_preshape)
            
        #     # Reset.
        #     raw_input("go home?")
        #     self.move_arm_home()

        return arm_traj

    def publish_transform(self, pose, name, base):
        br = tf.TransformBroadcaster()
        rate = rospy.Rate(10.0)
        while not rospy.is_shutdown():
            br.sendTransform((pose.position.x, pose.position.y, pose.position.z),
                             (pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w),
                             rospy.Time.now(),
                             name, base)
            rate.sleep()

    # Get grasp info:
    # There are three ways we represent a graspable object:
    # 1. True mesh: pose, mesh file.
    # 2. SDF Model: pose, embedding, scale.
    # 3. Grasp success model: pose, partial voxels, size. Note: this will eventually reuse SDF stuff.
    # Generate using Gazebo: load a mesh into Gazebo and segment point cloud.
    def get_grasp_obj_info(self, mesh, verbose=False):

        # Client to create SDF embedding.
        embed_client = EmbedClient()

        # 1. Generate the true mesh information.
        # Load mesh into gazebo.
        insert_success, object_pose = self.insert_object(object_name)
        if not insert_success:
            print("Problem inserting object. Continuing.")
            return None, None, None, None, None, None

        mesh_pose = self.get_pose_stamped_from_array(object_pose)
        mesh_pose = self.tf_listener.transformPose('lbr4_base_link', mesh_pose).pose
        mesh_pose = [mesh_pose.position.x, mesh_pose.position.y, mesh_pose.position.z, mesh_pose.orientation.x, mesh_pose.orientation.y, mesh_pose.orientation.z, mesh_pose.orientation.w]
        mesh_file = 'package://ll4ma_scene_description/data/meshes/' + mesh + '.stl'

        # 2. Get GraspObject.
        grasp_obj = None
        # Segment object in scene.
        object_segment_response = self.segment_object_client()
        retr = None
        while object_segment_response is None:
            retr = raw_input("retry? y/n")
            if retr == 'y':
                object_segment_response = self.segment_object_client()
            else:
                break
        if retr is not None and retr != 'y':
            return None, None, None, None, None, None
        grasp_obj = object_segment_response.obj

        # Find object frame and convert point cloud to that frame.
        aligned_object = align_object(grasp_obj, self.tf_listener)

        # Get transform to aligned object frame.
        quat = [aligned_object.pose.orientation.x, aligned_object.pose.orientation.y, aligned_object.pose.orientation.z, aligned_object.pose.orientation.w]
        aligned_transform = tf.transformations.quaternion_matrix(quat)
        aligned_transform[0,3] = aligned_object.pose.position.x
        aligned_transform[1,3] = aligned_object.pose.position.y
        aligned_transform[2,3] = aligned_object.pose.position.z
        aligned_transform = np.linalg.inv(aligned_transform)

        # Center point cloud in new frame.
        obj_cloud = np.array(list(pc2.read_points(grasp_obj.cloud, field_names = ("x", "y", "z"), skip_nans=True)))
        for i in range(len(obj_cloud)):
            obj_cloud[i] = np.dot(aligned_transform, [obj_cloud[i][0], obj_cloud[i][1], obj_cloud[i][2], 1.0])[:3]        
        
        # 3. Get SDF representations.
        # Embed the object.
        embedding, scale = embed_client.embed_point_cloud(obj_cloud, verbose)

        # Collect SDF representation.
        sdf_embedding = list(np.reshape(embedding, (-1)))

        # 4. Get Grasp Posterior representations.
        # Determine voxelization size.
        max_dim = max(
            np.amax(obj_cloud[:,0]) - np.amin(obj_cloud[:,0]),
            np.amax(obj_cloud[:,1]) - np.amin(obj_cloud[:,1]),
            np.amax(obj_cloud[:,2]) - np.amin(obj_cloud[:,2]),
        )
        voxel_size = max_dim / 26

        # Voxelize.
        voxels = point_cloud_to_voxel(obj_cloud, voxel_size, 26, 32, verbose)

        # 5. SDF/Grasp Posterior use the same frame.
        # Now get aligned object pose in the lbr4 base link frame.
        aligned_object_pose = PoseStamped()
        aligned_object_pose.header.frame_id = 'world'
        aligned_object_pose.pose = aligned_object.pose
        aligned_object_pose = grasp_client.tf_listener.transformPose('lbr4_base_link', aligned_object_pose).pose

        # Collect posterior representation.
        posterior_voxels = voxels.flatten()
        posterior_size = [aligned_object.width, aligned_object.height, aligned_object.depth]
        posterior_pose = [aligned_object_pose.position.x, aligned_object_pose.position.y, aligned_object_pose.position.z, aligned_object_pose.orientation.x, aligned_object_pose.orientation.y, aligned_object_pose.orientation.z, aligned_object_pose.orientation.w]
        sdf_pose = posterior_pose

        grasp_client.write_obj_info_to_yaml('/home/markvandermerwe/catkin_ws/src/ll4ma_scene_description/data/grasp_objs/' + mesh + '.yaml', sdf_pose, posterior_pose, posterior_size, posterior_voxels, sdf_embedding, scale, mesh_file, mesh_pose)

        return sdf_embedding, scale, sdf_pose, posterior_voxels, posterior_size, posterior_pose, mesh_file, mesh_pose
            
if __name__ == '__main__':

    rospy.init_node('reconstruction_grasp_client')

    load_cached_obj = rospy.get_param("use_obj_file")
    
    # Make client to perform the grasps.
    grasp_client = ReconstructionGraspClient()
    
    # Object dataset (YCB)
    dataset_dir = '/home/markvandermerwe/YCB/ycb'
    object_mesh_dirs = os.listdir(dataset_dir)
    #random.shuffle(object_mesh_dirs)
    object_mesh_dirs =  ['004_sugar_box'] # ['002_master_chef_can'] # ['036_wood_block']

    for i, object_name in enumerate(object_mesh_dirs):
        rospy.loginfo('Object: %s' %object_name)

        if not load_cached_obj:
            sdf_embedding, sdf_scale, sdf_pose, grasp_embedding, grasp_size, grasp_pose, true_mesh, true_pose = grasp_client.get_grasp_obj_info(mesh=object_name, verbose=False)
        else:
            # Read from cached file.
            sdf_embedding = rospy.get_param("ll4ma_scene/sdf_embedding")
            sdf_scale = rospy.get_param("ll4ma_scene/sdf_scale")
            sdf_pose = rospy.get_param("ll4ma_scene/sdf_pose")
            grasp_embedding = rospy.get_param("ll4ma_scene/grasp_embedding")
            grasp_size = rospy.get_param("ll4ma_scene/grasp_size")
            grasp_pose = rospy.get_param("ll4ma_scene/grasp_pose")
            true_mesh = rospy.get_param("ll4ma_scene/true_mesh")
            true_pose = rospy.get_param("ll4ma_scene/true_pose")
        
        grasp_client.update_ll4ma_planner(
            sdf_embedding,
            sdf_scale,
            sdf_pose,
            grasp_embedding,
            grasp_size,
            grasp_pose,
            true_mesh,
            true_pose)

        # Sample an initial setting: TODO: Swap w/ prior.
        # joint_settings = grasp_client.sample_grasp_preshape()
        # init_joint_traj = grasp_client.get_trajectory_from_joint_angles([joint_settings])
        # grasp_client.display_trajectory(init_joint_traj, "/seed_traj")
        
        # Get current hand/robot joint states.
        # robot_js, hand_js = grasp_client.get_joint_states()
        
        # Setup init trajectory.
        # init_j_angles = [0.0] * 23
        # init_j_angles[0] = -0.35
        # init_j_angles[1] = 1.19
        # init_j_angles[3] = -1.34
        # init_j_angles[5] = -0.85
        # init_j_angles[6] = 1.59
        # init_j_angles[19] = 1.06
        # robot_js.position = init_j_angles[:7]
        # hand_js.position = init_j_angles[7:]

        # fin_j_angles = [0.0] * 23
        # fin_j_angles[0] = 0.27
        # fin_j_angles[1] = 1.19
        # fin_j_angles[3] = -1.34
        # fin_j_angles[5] = -0.85
        # fin_j_angles[6] = 1.59
        # fin_j_angles[19] = 1.06

        init_j_angles = [0.0] * 7
        init_j_angles[0] = -0.26
        init_j_angles[1] = 2.15
        init_j_angles[3] = 0.43
        robot_js = JointState()
        robot_js.name=['lbr4_j0','lbr4_j1','lbr4_j2','lbr4_j3','lbr4_j4','lbr4_j5','lbr4_j6']
        robot_js.position=init_j_angles
        hand_js = JointState()

        fin_j_angles = [0.0] * 7
        fin_j_angles[0] = 0.34
        fin_j_angles[1] = 2.15
        fin_j_angles[3] = 0.43

        init_traj = grasp_client.get_trajectory_from_joint_angles([init_j_angles, fin_j_angles])
        grasp_client.display_trajectory(init_traj, "/seed_traj")

        # Setup preshape
        hand_preshape = JointState()
        # hand_preshape.name = ['index_joint_0', 'index_joint_1', 'index_joint_2', 'index_joint_3', 'middle_joint_0', 'middle_joint_1',
        #                       'middle_joint_2', 'middle_joint_3', 'ring_joint_0', 'ring_joint_1', 'ring_joint_2', 'ring_joint_3',
        #                       'thumb_joint_0', 'thumb_joint_1', 'thumb_joint_2', 'thumb_joint_3']
        # hand_preshape.position = [0.0] * 16
        # hand_preshape.position[12] = 1.06
        
        goal_palm_pose = Pose()
        goal_palm_pose.position.x = -0.78124
        goal_palm_pose.position.y = -0.28068
        goal_palm_pose.position.z = 0.1588
        goal_palm_pose.orientation.x = 0.12082
        goal_palm_pose.orientation.y = -0.69839
        goal_palm_pose.orientation.z = 0.12025
        goal_palm_pose.orientation.w = 0.69512

        start = time.time()
        full_plan = grasp_client.get_palm_plan_traj(robot_js, hand_js, goal_palm_pose, hand_preshape, init_traj)
        end = time.time()
        print("Planning time: ", (end-start), " sec.")
        print("Planning time: ", ((end-start)/60.), " min.")
        grasp_client.display_trajectory(full_plan.robot_joint_traj, '/robot_plan')
        
        raw_input("Wait.")
        
        # Remove the object from the scene.
        # grasp_client.remove_objects([object_name])
        
